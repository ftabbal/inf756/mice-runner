import QtQuick
import QtQuick.Controls
import QtWebSockets

import my.mazes

Window {
    width: 1280
    height: 720
    visible: true
    title: qsTr("MiceRunner")
    property int wallThickness: 5


    WebSocket {
        id: socket
        url: "ws://localhost:4444"
        onTextMessageReceived: {
            console.log(message)
        }
//            onStatusChanged: if (socket.status == WebSocket.Error) {
//                                 console.log("Error: " + socket.errorString)
//                             } else if (socket.status == WebSocket.Open) {
//                                 socket.sendTextMessage("Hello World")
//                             } else if (socket.status == WebSocket.Closed) {
//                                 messageBox.text += "\nSocket closed"
//                             }
        active: false
    }

}
