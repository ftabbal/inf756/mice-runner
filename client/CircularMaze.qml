import QtQuick
import QtQml
import my.mazes
import "utils.js" as Utils

Item {

//    signal neighbourSelected(PolarCell nextCell)
    signal pathReady(int mouseId, Path path, PolarCell cell)

    property int mouseCounter:0

    property var pathList: ({})

    Canvas {
        id: maze
        width: parent.width
        height: parent.width

        x: 0
        y: 0

        property int centerX: width / 2
        property int centerY: width / 2

        property var context

        property point center: Qt.point(width / 2, height / 2)
        property int cellSize

        onPaint: {
            context = getContext("2d")
            drawGraph()
            drawMaze()
//            drawPath(2, 1)
//            drawPath(3, 2)
//            drawPath(4, 3)
//            drawPath(5, 4)
//            drawPath(6, 5)
//            drawPath(1, 6)
//            drawPath(1, 2, "green")
//            drawPath(2, 3, "green")
//            drawPath(3, 4, "green")
//            drawPath(4, 5, "green")
//            drawPath(5, 6, "green")
//            drawPath(6, 1, "green")
//            drawPath(0, 5)
//            drawPath(1, 8)
//            drawPath(1, 2)
//            drawPath(2, 3)
//            drawPath(3, 12)
//            drawPath(4, 5)
//            drawPath(4, 14)
//            drawPath(5, 16)
//            drawPath(6, 17)
//            drawPath(12, 30)
//            drawPath(8, 22)
//            drawPath(67, 92)
        }
//        Component.onCompleted: context = getContext("2d")

    }

    Component {
        id: pathComponent
        Path {

        }
    }

    Component {
        id: pathArcComponent
        PathArc {}
    }

    Component {
        id: pathLineComponent
        PathLine {}
    }

    Component {
        id: mouseComponent
        Mouse {
            id: mouseItem
        }

    }

    Component {
        id: foodComponent
        FoodItem {
            id: foodItem
        }
    }

//    Rectangle {
//        id: character

//        width: maze.cellSize / 3
//        height: maze.cellSize / 3
//        color: "blue"
//        x: maze.center.x - width / 2
//        y: maze.center.y -height / 2


//    }

    Circular {
        id: graph

        Component.onCompleted: {
            var path = ":/data/circle/data/circle/%1.txt"
            loadFile(path.arg("5"))
            console.log("Num levels:", numLevels)
            maze.cellSize = maze.width / (2 * numLevels)
            buildPathList()
            maze.requestPaint()
        }

    }

    function buildPathList() {
        for (var i = 0; i < graph.cells.length; ++i) {
            var cell = graph.cells[i]
            pathList[cell] = {}
            for (var j = 0; j < cell.neighbours.length; ++j) {
                var neighbour = cell.neighbours[j]
                if (graph.hasConnectionBetween(cell, neighbour)) {
                    pathList[cell][neighbour] = makePath(cell, neighbour)
                }
            }
        }
    }

    function getCellCenter(cell) {
        var radius = cell.level * maze.cellSize
        return Utils.getPointFromAngle(maze.center, radius, cell.angle)
    }

    function pickRandomNeighbour(cell, prevCell) {
        var neighbours = cell.neighbours
//        console.log("Neighbours for %1:".arg(cell.id))
        var connectedNeighbours = []

        for (var i = 0; i< neighbours.length; ++i) {
//            console.log("\t%1".arg(neighbours[i].id))
            if (graph.hasConnectionBetween(cell, neighbours[i]))
                connectedNeighbours.push(neighbours[i])
        }
        if (connectedNeighbours.length === 1)
            return connectedNeighbours[0]

        while (true) {
            var index = Utils.getRandomIndex(connectedNeighbours)
            var nextCell = connectedNeighbours[index]
            if (nextCell.id !== prevCell.id)
                return nextCell
        }
    }

    function drawPath(srcId, targetId, color = "blue", lineThickness = 1) {
        var cellList = graph.cells
        var srcCell = cellList[srcId]
        var targetCell = cellList[targetId]
        var path = makePath(srcCell, targetCell)
        maze.context.save()
        maze.context.strokeStyle = color
        maze.context.path = path
        maze.context.stroke()
    }

    function makePath(srcCell, targetCell) {
        var srcPoint = getCellCenter(srcCell)
        var targetPoint = getCellCenter(targetCell)
        var movePath = pathComponent.createObject(maze)
        movePath.startX = srcPoint.x
        movePath.startY = srcPoint.y
        var srcRadius = srcCell.level * maze.cellSize
        var targetRadius = targetCell.level * maze.cellSize
        var sameLevel = srcCell.level === targetCell.level

        if (sameLevel) {
            var pathArc = pathArcComponent.createObject(movePath)
            pathArc.x = targetPoint.x
            pathArc.y = targetPoint.y
            pathArc.radiusX = srcRadius
            pathArc.radiusY = srcRadius

            var isClockwise = srcCell.index > targetCell.index && targetCell.index !== 0
                    || srcCell.index === 0 && targetCell.angle > 3 * Math.PI / 2
                    || targetCell.index === 0 && srcCell.angle < Math.PI / 2

            if (!isClockwise)
                pathArc.direction = PathArc.Counterclockwise
            else
                pathArc.direction = PathArc.Clockwise
            movePath.pathElements.push(pathArc)
        } else {
            if (srcCell.angle === targetCell.angle || srcCell === graph.cells[0] || targetCell === graph.cells[0]) {
                var pathLine = pathLineComponent.createObject(movePath)
                pathLine.x = targetPoint.x
                pathLine.y = targetPoint.y
                movePath.pathElements.push(pathLine)
            } else {
                var srcAngleSize = 2 * Math.PI / graph.numCellsForLevel(srcCell.level)
                var targetAngleSize = 2 * Math.PI / graph.numCellsForLevel(targetCell.level)
                var srcCellOffset = 0
                var targetCellOffset = 0
                var isClockwise = !(srcCell.angle < Math.PI / 2 && targetCell.angle > 3 * Math.PI / 2) && (targetCell.angle < srcCell.angle)
                var isTowardsInner = srcCell.level > targetCell.level
                var srcAngleOffset = 0
                var targetAngleOffset = 0
                if (isTowardsInner) {
                    srcAngleOffset = srcAngleSize / 4
                    targetAngleOffset = targetAngleSize / 3
                } else {
                    srcAngleOffset = srcAngleSize / 3
                    targetAngleOffset = targetAngleSize / 4
                }

                var arcSectionStart = pathArcComponent.createObject(movePath)
                var lineSectionToLevel = pathLineComponent.createObject(movePath)
                var arcSectionEnd = pathArcComponent.createObject(movePath)
                if (srcCell.angle > targetCell.angle) {
                    srcCellOffset = Utils.getPointFromAngle(maze.center, srcRadius, srcCell.angle - srcAngleOffset);
                    targetCellOffset = Utils.getPointFromAngle(maze.center, targetRadius, targetCell.angle + targetAngleOffset)
                }  else {
                    srcCellOffset = Utils.getPointFromAngle(maze.center, srcRadius, srcCell.angle + srcAngleOffset);
                    targetCellOffset = Utils.getPointFromAngle(maze.center, targetRadius, targetCell.angle - targetAngleOffset)
                }
                arcSectionStart.x = srcCellOffset.x
                arcSectionStart.y = srcCellOffset.y
                arcSectionStart.radiusX = srcPoint.x
                arcSectionStart.radiusY = srcPoint.y
                lineSectionToLevel.x = targetCellOffset.x
                lineSectionToLevel.y = targetCellOffset.y
                arcSectionEnd.x = targetPoint.x
                arcSectionEnd.y = targetPoint.y
                arcSectionEnd.radiusX = targetPoint.x
                arcSectionEnd.radiusY = targetPoint.y

                if (isClockwise) {
                    arcSectionStart.direction = PathArc.Clockwise
                    arcSectionEnd.direction = PathArc.Clockwise
                } else {
                    arcSectionStart.direction = PathArc.Counterclockwise
                    arcSectionEnd.direction = PathArc.Counterclockwise
                }
                movePath.pathElements.push(arcSectionStart)
                movePath.pathElements.push(lineSectionToLevel)
                movePath.pathElements.push(arcSectionEnd)
            }
        }
        return movePath
    }

    function getPath(mouseId, currCell, prevCell) {
        try {
            var neighbour = pickRandomNeighbour(currCell, prevCell)
            var path = pathList[currCell][neighbour] //(currCell, neighbour)
            pathReady(mouseId, path, neighbour)
            return path
        } catch(error) {
            print ("Error (getPath): %1".arg(error.toString()))
//            for (var i = 0; i < error.qmlErrors.length; i++) {
//                print("lineNumber: " + error.qmlErrors[i].lineNumber)
//                print("columnNumber: " + error.qmlErrors[i].columnNumber)
//                print("fileName: " + error.qmlErrors[i].fileName)
//                print("message: " + error.qmlErrors[i].message)
//            }
        }
    }

    function loadFood(foodItem, cell) {
        var point = getCellCenter(cell)
//        console.log("component ready")
//        console.log("Placing it at cell %1: (%2, %3)".arg(cell.id).arg(point.x).arg(point.y))
        if (foodItem.status === Component.Ready) {
            var res = foodItem.createObject(maze)
            res.width = maze.cellSize / 4
            res.height = maze.cellSize / 4
            res.x = point.x - res.width / 2
            res.y = point.y - res.height / 2
            res.icon.source = "qrc://Assets/food/Carrot.png"
//            console.log("Image added!")
            if (res === null) {
                console.log("Error creating food item");
            }
        } else if (foodItem.status === Component.Error) {
            console.log("Error loading component:", foodItem.errorString());
        }
    }

    function addFood() {
//        console.log("Calling addFood...")
        var cellIndex = Utils.getRandomIndex(graph.cells)
        var cell = graph.cells[cellIndex]
        var point = getCellCenter(cell)
        var foodItem = foodComponent.createObject(parent)
        foodItem.width = maze.cellSize / 3
        foodItem.height = maze.cellSize / 2
        foodItem.x = point.x - foodItem.width / 2
        foodItem.y = point.y - foodItem.height / 2
//        if (foodItem.status === Component.Ready || foodItem.status === Component.Error) {
//            loadFood(foodItem, cell)
//        } else {
//            foodItem.statusChanged.connect(loadFood(foodItem, cell))
//        }
    }

//    function initMouse()

    function addMouse() {
        var cellIndex = Utils.getRandomIndex(graph.cells)
        var cell = graph.cells[cellIndex]
        mouseCounter += 1
        var mouse = mouseComponent.createObject(maze, {currentCell: cell,
                                                    previousCell: cell,
                                                    identifier: mouseCounter
                                                })
        mouse.width = maze.cellSize / 3
        mouse.height = maze.cellSize / 3
        mouse.x = maze.center.x - mouse.width / 2
        mouse.y = maze.center.y - mouse.height / 2
        mouse.requestPath.connect(getPath)
        pathReady.connect(mouse.onPathReceived)
        console.debug("Mouse created! Placed at: (%1, %2)".arg(mouse.currentCell.level).arg(mouse.currentCell.index))
        mouse.requestPath(mouse.identifier, mouse.currentCell, mouse.previousCell)
//        console.log("Calling addMouse")
//        var component = Qt.createComponent("Mouse.qml")
//        if (component.status === Component.Ready || component.status === Component.Error) {
//            loadMouse(component)
//        } else {
//            component.statusChanged.connect(loadMouse(component))
//        }
    }

    function drawGraph() {
        maze.context.save()
        var cellList = graph.cells;
        Utils.drawDot(maze.context, maze.center)
        Utils.drawText(maze.context, maze.center, cellList[0].index)
        for (var i = 1; i < cellList.length; ++i) {
            var cell = cellList[i]

            var radius = cell.level * maze.cellSize
            var angle = cell.angle
            var cellCenterPoint = Utils.getPointFromAngle(maze.center, radius, angle)

            // Check and connect if needed the neighbour couter clockwise
            var ccw = cell.ccwCell
            if (cell !== null && ccw !== null) {
                var connectedToCcw = graph.hasConnectionBetween(cell, ccw)
                if (connectedToCcw) {
                    drawPath(cell.id, ccw.id)
                    Utils.drawArc(maze.context, maze.center, cell.angle, ccw.angle, radius, "red")
                }
            }

            // Check and connect if needed the inward cells
            var inwardCells = cell.inwardCells
            var inwardRadius = radius - maze.cellSize
            var angleSize = 2 * Math.PI / graph.numCellsForLevel(cell.level)
            var inwardAngleSize = 2 * Math.PI / graph.numCellsForLevel(cell.level - 1)
            var outwardCells = cell.outwardCells


            for (var c = 0; c < inwardCells.length; ++c) {
                var inCell = inwardCells[c]

                if (graph.hasConnectionBetween(cell, inCell)) {
                    drawPath(cell.id, inCell.id)
                    if (cell.angle !== inCell.angle && inCell.level !== 0) {
                        var currCellOffset = 0
                        var inwardOffset = 0
                        if (cell.angle > inCell.angle) {
                            Utils.drawArc(maze.context, maze.center, cell.angle - angleSize / 4, cell.angle, radius, "red")
                            Utils.drawArc(maze.context, maze.center, inCell.angle, inCell.angle + inwardAngleSize / 3, inwardRadius, "red")
                            currCellOffset = Utils.getPointFromAngle(maze.center, radius, cell.angle - angleSize /4);
                            inwardOffset = Utils.getPointFromAngle(maze.center, inwardRadius, inCell.angle + inwardAngleSize /3)
                            Utils.drawLine(maze.context, currCellOffset, inwardOffset, "red")
                        }  else {
                            Utils.drawArc(maze.context, maze.center, cell.angle, cell.angle + angleSize / 4, radius, "red")
                            Utils.drawArc(maze.context, maze.center, inCell.angle - inwardAngleSize / 3, inCell.angle, inwardRadius, "red")
                            currCellOffset = Utils.getPointFromAngle(maze.center, radius, cell.angle + angleSize /4);
                            inwardOffset = Utils.getPointFromAngle(maze.center, inwardRadius, inCell.angle - inwardAngleSize /3)
                            }

                        Utils.drawLine(maze.context, currCellOffset, inwardOffset, "red")


                    } else {
                        var start = Utils.getPointFromAngle(maze.center, inCell.level * maze.cellSize, inCell.angle);
                        var end = Utils.getPointFromAngle(maze.center, radius, cell.angle)
                        drawPath(cell.id, inCell.id)
                        Utils.drawLine(maze.context, start, end, "red")
                    }
                }
            }

            Utils.drawDot(maze.context, cellCenterPoint)
            Utils.drawText(maze.context, cellCenterPoint, cell.id)
            Utils.drawText(maze.context, Qt.point(cellCenterPoint.x, cellCenterPoint.y + 20), cell.index, "red")
        }

        var centerCell = cellList[0]
    }

    function drawCell(context, cellNumAtLevel, cellLevel, cellCenterX, cellCenterY) {
        context.save()
        var numCellsForLevel = graph.numCellsForLevel(cellLevel)
        var radius = cellLevel * maze.cellSize
        var innerRadius = radius - maze.cellSize /2 // inner wall
        var outerRadius = radius + maze.cellSize /2 // outer wall
        var angleSize = 2 * Math.PI / numCellsForLevel
        var theta = cellNumAtLevel * angleSize

        // center of cell is the bisector
        var theta_cw =  theta - angleSize / 2 // clockwise
        var theta_ccw = theta + angleSize / 2 // counter-clockwise

//        var thicknessOffset =

        // see Maze for Programmers p. 99 for description of these values
        var a = Utils.getPointFromAngle(maze.center, innerRadius, theta_cw)
        var b = Utils.getPointFromAngle(maze.center, outerRadius, theta_cw)
        var c = Utils.getPointFromAngle(maze.center, innerRadius, theta_ccw)
        var d = Utils.getPointFromAngle(maze.center, outerRadius, theta_ccw)

        Utils.drawLine(context, c, d, "green", 3)   // closer to 0-axis
        Utils.drawLine(context, a, b, "purple", 3)      // farther to 0-axis
        Utils.drawArc(context, maze.center, theta_cw, theta_ccw, innerRadius - 3, "green", 3)
        Utils.drawArc(context, maze.center, theta_cw, theta_ccw, outerRadius, "purple", 3)
    }

    function drawMaze() {
        maze.context.save()
        var cellList = graph.cells
        for (var i = 1; i < cellList.length; ++i) {
            var cell = cellList[i]
            var radius = cell.level * maze.cellSize
            var angleSize = 2 * Math.PI / graph.numCellsForLevel(cell.level)
            var innerRadius = cell.level * maze.cellSize - maze.cellSize /2 // inner wall
            var outerRadius = cell.level * maze.cellSize + maze.cellSize /2 // outer wall

            var theta_cw = (cell.angle - angleSize / 2)
            var theta_ccw = (cell.angle + angleSize / 2)

            var a = Utils.getPointFromAngle(maze.center, innerRadius, theta_cw)
            var b = Utils.getPointFromAngle(maze.center, outerRadius, theta_cw)
            var c = Utils.getPointFromAngle(maze.center, innerRadius, theta_ccw)
            var d = Utils.getPointFromAngle(maze.center, outerRadius, theta_ccw)

            var ccwCell = cell.ccwCell

            if (!graph.hasConnectionBetween(cell, ccwCell)) {
                Utils.drawLine(maze.context, c, d, "black", 3)
            }

            var inwardCells = cell.inwardCells
            var inwardRadius = radius - maze.cellSize

            if (inwardCells.length === 1 && !graph.hasConnectionBetween(cell, inwardCells[0])) {
                Utils.drawArc(maze.context, maze.center, theta_cw, theta_ccw, innerRadius, "black", 3)
                var anotherPoint = Utils.getPointFromAngle(maze.center, innerRadius, cell.angle)
//                Utils.drawText(context, anotherPoint, "%1 - %2".arg(inwardCells[0].id).arg(cell.id), "green")
            } else {
                for (var j = 0; j < inwardCells.length; ++j) {
                    var inwardCell = inwardCells[j]
                    if (!graph.hasConnectionBetween(cell, inwardCell)) {
                        var inwardIsCw = !(inwardCell.angle < Math.PI / 2 && cell.angle > 3 * Math.PI / 2) && (inwardCell.angle < cell.angle)
//                        console.log("cell: %1 (%2), inwardCell: %3 (%4), isCw: %5"
//                                    .arg(cell.id).arg(cell.angle)
//                                    .arg(inwardCell.id).arg(inwardCell.angle)
//                                    .arg(inwardIsCw))
                        if (inwardIsCw) {
                            Utils.drawArc(maze.context, maze.center, theta_cw, cell.angle, innerRadius, "black", 3)
                        } else {
                            Utils.drawArc(maze.context, maze.center, cell.angle, theta_ccw, innerRadius, "black", 3)
                        }
                        var aPoint = Utils.getPointFromAngle(maze.center, innerRadius, cell.angle)
//                        Utils.drawText(context, aPoint, "%1 - %2".arg(inwardCell.id).arg(cell.id), "blue")
                    }
                }
            }
        }

        Utils.drawCircle(maze.context, maze.center, (graph.numLevels - 1) * maze.cellSize + maze.cellSize / 2, "black", 3)

    }


}
