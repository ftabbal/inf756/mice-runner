// Adapted from https://www.qt.io/product/qt6/qml-book/ch13-networking-rest-api

var BASE = "http://localhost:3001"
function request(verb, endpoint, obj, callback) {
    print('request: ' + verb + ' ' + BASE + (endpoint ? '/' + endpoint : ''))
    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {
//        print('xhr: on ready state change: ' + xhr.readyState)
//        console.log("xhr status: %1".arg(xhr.status))
//        console.log(xhr.responseText)
        if(xhr.readyState === XMLHttpRequest.DONE) {

            if(callback) {
                var res = JSON.parse(xhr.responseText.toString())
                callback(res)
            }
        }
    }
    xhr.open(verb, BASE + (endpoint ? '/' + endpoint : ''))
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.setRequestHeader('Accept', 'application/json')
    var data = obj ? JSON.stringify(obj) : ''
    xhr.send(data)
}

function getMaze(shape, size, algo, callBackFunction) {
    request("GET", "maze?shape=%1&algo=%2&size=%3".arg(shape).arg(algo).arg(size), null, callBackFunction)
}
