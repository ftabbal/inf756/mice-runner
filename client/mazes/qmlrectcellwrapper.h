#ifndef MAZE_RECTANGLE_CELL_H
#define MAZE_RECTANGLE_CELL_H

#include "qqmlintegration.h"
#include <QObject>

#include "grids/rectangle/rectcell.h"

namespace Maze {
namespace Rectangle {

class QmlCellWrapper : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(int xPos READ xPos CONSTANT)
    Q_PROPERTY(int yPos READ yPos CONSTANT)
    Q_PROPERTY(int id READ id CONSTANT)
public:
    explicit QmlCellWrapper(Maze::Grid::RectCell* cell, QObject *parent = nullptr);
    int xPos() const { return m_cell->xPos();}
    int yPos() const { return m_cell->yPos();}
    int id() const { return m_cell->id();}

    Maze::Grid::RectCell* cell() const {return m_cell;}

signals:

protected:
    Maze::Grid::RectCell* m_cell;




};

} // namespace Rectangle
} // namespace Maze

Q_DECLARE_METATYPE(Maze::Rectangle::QmlCellWrapper)
Q_DECLARE_METATYPE(Maze::Rectangle::QmlCellWrapper*)

#endif // MAZE_RECTANGLE_CELL_H
