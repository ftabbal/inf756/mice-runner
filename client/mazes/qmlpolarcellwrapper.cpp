#include "qmlpolarcellwrapper.h"

namespace Maze {
namespace Polar {

QmlCellWrapper::QmlCellWrapper(Maze::Grid::PolarCell* cell, QObject *parent)
    : QObject{parent}, m_cell(cell)
{

}

} // namespace Polar
} // namespace Maze
