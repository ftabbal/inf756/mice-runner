#include "circular.h"

#include <QtMath>
#include <QFile>
#include <QTextStream>

#include <numeric>

#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>

namespace Maze {

void Circular::load(const QJsonObject& settings) {
//    clear();
    m_grid.init(settings["size"].toObject()["radius"].toInt());
    m_grid.load(settings);
    convertGridCells();
}

void Circular::convertGridCells() {
    for (auto& cell : m_grid.cells()) {
        auto res = dynamic_cast<Maze::Grid::PolarCell*>(cell);
        m_cells.append(QVariant::fromValue(new Polar::QmlCellWrapper(res, this)));
    }
}

std::vector<Polar::QmlCellWrapper*> Circular::getConnections(Polar::QmlCellWrapper* cell) const {
    auto gridCell = cell->cell();
    std::vector<Polar::QmlCellWrapper*> connections;
    for (auto c : gridCell->connections()) {
        auto conn = m_cells[c->id()].value<Polar::QmlCellWrapper*>();
        connections.push_back(conn);
    }
    return connections;
}

bool Circular::areConnected(Polar::QmlCellWrapper* cell1, Polar::QmlCellWrapper* cell2) const {
    auto trueCell1 = cell1->cell();
    auto trueCell2 = cell2->cell();
    return trueCell1->connections().contains(trueCell2);
}

Polar::QmlCellWrapper* Circular::ccwCellFor(Polar::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto n = trueCell->ccwCell();
    if (n)
        return m_cells[n->id()].value<Polar::QmlCellWrapper*>();
    return nullptr;
}

Polar::QmlCellWrapper* Circular::cwCellFor(Polar::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto n = trueCell->cwCell();
    if (n)
        return m_cells[n->id()].value<Polar::QmlCellWrapper*>();
    return nullptr;

}

std::vector<Polar::QmlCellWrapper*> Circular::inwardCellsFor(Polar::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto inwardSet = trueCell->inwardCells();
    std::vector<Polar::QmlCellWrapper*> inwards;

    for (auto n : inwardSet) {
        auto val = m_cells[n->id()].value<Polar::QmlCellWrapper*>();
        inwards.push_back(val);
    }

    return inwards;
}

std::vector<Polar::QmlCellWrapper*> Circular::outwardCellsFor(Polar::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto outwardSet = trueCell->outwardCells();
    std::vector<Polar::QmlCellWrapper*> outwards;

    for (auto n : outwardSet) {
        auto val = m_cells[n->id()].value<Polar::QmlCellWrapper*>();
        outwards.push_back(val);
    }

    return outwards;
}

std::vector<Polar::QmlCellWrapper*> Circular::neighboursFor(Polar::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto neighboursSet = trueCell->neighbours();
    std::vector<Polar::QmlCellWrapper*> neighbours;

    for (auto n : neighboursSet) {
        auto val = m_cells[n->id()].value<Polar::QmlCellWrapper*>();
        neighbours.push_back(val);
    }

    return neighbours;
}

Polar::QmlCellWrapper* Circular::getRandomCell() const {
    auto c = m_grid.randomCell();
    return m_cells[c->id()].value<Polar::QmlCellWrapper*>();
}

QStringList Circular::toAdjacencyList() const {
    return m_grid.adjacencyList();
}

} // namespace Maze
