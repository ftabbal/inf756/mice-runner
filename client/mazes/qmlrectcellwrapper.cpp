#include "qmlrectcellwrapper.h"

namespace Maze {
namespace Rectangle {

QmlCellWrapper::QmlCellWrapper(Maze::Grid::RectCell* cell, QObject *parent)
    : QObject{parent}, m_cell(cell)
{

}

} // namespace Rectangle
} // namespace Maze
