#ifndef MAZE_POLAR_QMLCELLWRAPPER_H
#define MAZE_POLAR_QMLCELLWRAPPER_H

#include "qqmlintegration.h"
#include <QObject>

#include "grids/polar/polarcell.h"

namespace Maze {
namespace Polar {

class QmlCellWrapper : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(int id READ id CONSTANT)
    Q_PROPERTY(double angle READ angle CONSTANT)
    Q_PROPERTY(int level READ level CONSTANT)
    Q_PROPERTY(int index READ index CONSTANT)
public:
    explicit QmlCellWrapper(Maze::Grid::PolarCell* cell, QObject *parent = nullptr);
    int id() const { return m_cell->id(); }
    double angle() const { return m_cell->angle(); }
    int level() const { return m_cell->level(); }
    int index() const { return m_cell->index();}

    Maze::Grid::PolarCell* cell() const { return m_cell; }

signals:

protected:
    Maze::Grid::PolarCell* m_cell;

};

} // namespace Polar
} // namespace Maze

Q_DECLARE_METATYPE(Maze::Polar::QmlCellWrapper)
Q_DECLARE_METATYPE(Maze::Polar::QmlCellWrapper*)

#endif // MAZE_POLAR_QMLCELLWRAPPER_H
