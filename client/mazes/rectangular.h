#ifndef MAZE_RECTANGULAR_H
#define MAZE_RECTANGULAR_H

#include <QtQmlIntegration>

#include <QObject>
#include "grids/rectangle/rectangle.h"
#include "qmlrectcellwrapper.h"

namespace Maze {

class Rectangular : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QVariantList cells READ cells CONSTANT)
    Q_PROPERTY(int width READ width CONSTANT)
    Q_PROPERTY(int height READ height CONSTANT)
public:
    explicit Rectangular(QObject* parent = nullptr);

    Q_INVOKABLE void load(const QJsonObject& settings);
    Q_INVOKABLE std::vector<Rectangle::QmlCellWrapper*> getConnections(Rectangle::QmlCellWrapper* cell) const;
    Q_INVOKABLE bool areConnected(Rectangle::QmlCellWrapper* cell1, Rectangle::QmlCellWrapper* cell2) const;
    QVariantList cells() const { return m_cells; }

    int width() const { return m_grid.width(); }
    int height() const { return m_grid.height(); }

    Q_INVOKABLE Rectangle::QmlCellWrapper* leftNeighbour(Rectangle::QmlCellWrapper* cell) const;
    Q_INVOKABLE Rectangle::QmlCellWrapper* rightNeighbour(Rectangle::QmlCellWrapper* cell) const;
    Q_INVOKABLE Rectangle::QmlCellWrapper* upNeighbour(Rectangle::QmlCellWrapper* cell) const;
    Q_INVOKABLE Rectangle::QmlCellWrapper* downNeighbour(Rectangle::QmlCellWrapper* cell) const;
    Q_INVOKABLE Rectangle::QmlCellWrapper* getRandomCell() const;
    Q_INVOKABLE QStringList toAdjacencyList() const;


protected:
    Maze::Grid::Rectangle m_grid;
    QVariantList m_cells;
    int m_totalCells;

    void convertGridCells();

    // Accessors

};

} // namespace Maze

#endif // MAZE_RECTANGULAR_H
