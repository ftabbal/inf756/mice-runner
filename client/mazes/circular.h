#ifndef MAZE_CIRCULAR_H
#define MAZE_CIRCULAR_H

#include "qqmlintegration.h"

#include <QObject>
#include <QJsonObject>

#include <vector>
#include <map>
#include <set>

#include <QVariantList>
#include <QVector>

#include "grids/polar/polar.h"
#include "qmlpolarcellwrapper.h"

namespace Maze {

class Circular : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QVariantList cells READ cells CONSTANT)
    Q_PROPERTY(int radius READ radius CONSTANT)

public:
    explicit Circular(QObject* parent = nullptr): QObject(parent), m_grid(this) { }

    Q_INVOKABLE void load(const QJsonObject& settings);
    Q_INVOKABLE std::vector<Polar::QmlCellWrapper*> getConnections(Polar::QmlCellWrapper* cell) const;
    int totalCells() const { return m_totalCells; }
    /*Q_INVOKABLE*/ std::vector<int> numCellsAtLevel() const { return m_numCellsAtLevel; }

    Q_INVOKABLE int numCellsForLevel(int level) const {return m_grid.numCellsForLevel(level);}

    int radius() const { return m_grid.size(); }

    Q_INVOKABLE bool areConnected(Maze::Polar::QmlCellWrapper* cell1, Maze::Polar::QmlCellWrapper* cell2) const;
    QVariantList cells() const { return m_cells; }

    Q_INVOKABLE Maze::Polar::QmlCellWrapper* getRandomCell() const;
    Q_INVOKABLE QStringList toAdjacencyList() const;

    Q_INVOKABLE Polar::QmlCellWrapper* ccwCellFor(Polar::QmlCellWrapper* cell) const;
    Q_INVOKABLE Polar::QmlCellWrapper* cwCellFor(Polar::QmlCellWrapper* cell) const;
    Q_INVOKABLE std::vector<Polar::QmlCellWrapper*> inwardCellsFor(Polar::QmlCellWrapper* cell) const;
    Q_INVOKABLE std::vector<Polar::QmlCellWrapper*> outwardCellsFor(Polar::QmlCellWrapper* cell) const;
    Q_INVOKABLE std::vector<Polar::QmlCellWrapper*> neighboursFor(Polar::QmlCellWrapper* cell) const;

signals:

protected:
    Maze::Grid::Polar m_grid;
    QVariantList m_cells;
    int m_totalCells;

    std::vector<int> m_numCellsAtLevel;

    std::map<int, std::set<int>> m_graph;
    void convertGridCells();
//    void cellCountByLevel();
//    void parseRow(const QString& row);

};

} // namespace Maze


#endif // MAZE_CIRCULAR_H
