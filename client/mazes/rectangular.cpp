#include "rectangular.h"

#include "qmlrectcellwrapper.h"

#include <QDebug>

namespace Maze {

Rectangular::Rectangular(QObject *parent) : QObject(parent)
{

}

void Rectangular::load(const QJsonObject& settings) {
//    clear();
    m_grid.init(settings["size"].toObject()["h"].toInt(),
            settings["size"].toObject()["w"].toInt());
    m_grid.load(settings);
    convertGridCells();
}

void Rectangular::convertGridCells() {
    for (auto cell : m_grid.cells()) {
        auto res = dynamic_cast<Maze::Grid::RectCell*>(cell);
        m_cells.append(QVariant::fromValue(new Rectangle::QmlCellWrapper(res, this)));
    }
}

std::vector<Rectangle::QmlCellWrapper*> Rectangular::getConnections(Rectangle::QmlCellWrapper* cell) const {
    auto gridCell = cell->cell();
    std::vector<Rectangle::QmlCellWrapper*> connections;
    for (auto c : gridCell->connections()) {
        auto conn = m_cells[c->id()].value<Rectangle::QmlCellWrapper*>();
        connections.push_back(conn);
    }
    return connections;
}

bool Rectangular::areConnected(Rectangle::QmlCellWrapper* cell1, Rectangle::QmlCellWrapper* cell2) const {
    auto trueCell1 = cell1->cell();
    auto trueCell2 = cell2->cell();
    return trueCell1->connections().contains(trueCell2);
}

Rectangle::QmlCellWrapper* Rectangular::leftNeighbour(Rectangle::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto n = trueCell->leftCell();
    if (n)
        return m_cells[n->id()].value<Rectangle::QmlCellWrapper*>();
    return nullptr;
}
Rectangle::QmlCellWrapper* Rectangular::rightNeighbour(Rectangle::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto n = trueCell->rightCell();
    if (n)
        return m_cells[n->id()].value<Rectangle::QmlCellWrapper*>();
    return nullptr;
}

Rectangle::QmlCellWrapper* Rectangular::upNeighbour(Rectangle::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto n = trueCell->upCell();
    if (n)
        return m_cells[n->id()].value<Rectangle::QmlCellWrapper*>();
    return nullptr;
}

Rectangle::QmlCellWrapper* Rectangular::downNeighbour(Rectangle::QmlCellWrapper* cell) const {
    auto trueCell = cell->cell();
    auto n = trueCell->downCell();
    if (n)
        return m_cells[n->id()].value<Rectangle::QmlCellWrapper*>();
    return nullptr;
}

Rectangle::QmlCellWrapper* Rectangular::getRandomCell() const {
    auto c = m_grid.randomCell();
    return m_cells[c->id()].value<Rectangle::QmlCellWrapper*>();
}

QStringList Rectangular::toAdjacencyList() const {
    return m_grid.adjacencyList();
}

} // namespace Maze
