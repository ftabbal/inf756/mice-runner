QT += quick websockets

CONFIG += c++20

INCLUDEPATH += \
    mazes \
    mazes/Polar \
    ui/settings \
    ./

SOURCES += \
        main.cpp \
#        mazes/Polar/cell.cpp \
#        mazes/Polar/grid.cpp \
        mazes/circular.cpp \
        mazes/qmlpolarcellwrapper.cpp \
        mazes/qmlrectcellwrapper.cpp \
        mazes/rectangular.cpp

resources.files = main.qml \
    FoodItem.qml \
    FoodListModel.qml \
    Mouse.qml \
    ServerConnection.qml \
    api/setup_service.js \
    ui/controls/MouseDetailsPanel.qml \
    ui/controls/MouseGrid.qml \
    ui/controls/MousePanel.qml \
    ui/mazes/RectMaze.qml \
    ui/settings/FoodSettings.qml \
    ui/settings/Settings.qml \
    ui/settings/MiceSettings.qml \
    ui/settings/RuleSettings.qml \
    ui/settings/SettingListView.qml \
    ui/settings/maze/SizeSettings.qml \
    ui/settings/maze/ShapeSettings.qml \
    ui/settings/maze/AlgorithmSettings.qml \
    ui/settings/MazeSettings.qml \
    utils.js

RESOURCES += resources \
    assets.qrc \
    maze.qrc

#TRANSLATIONS += \
#    MazeRendererTest_fr_CA.ts
CONFIG += lrelease
CONFIG += embed_translations
#CONFIG += sanitizer sanitize_address
#CONFIG += sanitizer sanitize_memory
#CONFIG += sanitizer sanitize_thread
#CONFIG += sanitizer sanitize_undefined

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    mazes/circular.h \
    mazes/qmlpolarcellwrapper.h \
    mazes/qmlrectcellwrapper.h \
    mazes/rectangular.h

#CONFIG += qmltypes
#QML_IMPORT_NAME = my.mazes
#QML_IMPORT_MAJOR_VERSION = 1

DISTFILES += \
    FoodItem.qml \
    FoodListModel.qml \
    Mouse.qml \
    ServerConnection.qml \
    api/setup_service.js \
    ui/controls/MouseDetailsPanel.qml \
    ui/controls/MouseGrid.qml \
    ui/controls/MousePanel.qml \
    ui/mazes/RectMaze.qml \
    ui/settings/FoodSettings.qml \
    ui/settings/Settings.qml \
    ui/settings/MiceSettings.qml \
    ui/settings/RuleSettings.qml \
    ui/settings/SettingListView.qml \
    ui/settings/maze/SizeSettings.qml \
    ui/settings/maze/ShapeSettings.qml \
    ui/settings/maze/AlgorithmSettings.qml \
    ui/settings/MazeSettings.qml \
    utils.js

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../maze-lib/release/ -lmaze-lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../maze-lib/debug/ -lmaze-lib
else:unix: LIBS += -L$$OUT_PWD/../maze-lib/ -lmaze-lib

INCLUDEPATH += $$PWD/../maze-lib
DEPENDPATH += $$PWD/../maze-lib
