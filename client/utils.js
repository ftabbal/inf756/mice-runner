function drawDot(context, aPoint) {
    var dotRadius = 5;
    context.beginPath()
    context.ellipse(aPoint.x - dotRadius, aPoint.y - dotRadius, 2 * dotRadius, 2 * dotRadius)
    context.fill()
    context.closePath()
}

function drawCircle(context, origin, radius, lineColor="black") {
    context.beginPath()
    context.lineWidth = 2
    context.strokeStyle = lineColor
    context.arc(origin.x, origin.y, radius, 0, 2*Math.PI, true)
    context.stroke()
    context.closePath()
}

function drawText(context, aPoint, text, textColor="purple") {
    context.beginPath()
    context.lineWidth = 1
    context.strokeStyle = textColor
    context.strokeText(text, aPoint.x - 5, aPoint.y - 5)
    context.closePath()
}

function drawArc(context, origin, startingAngle, endingAngle, radius, lineColor = "black", lineWidth = 2) {
//        var message = "Drawing arc with center (%1, %2) starting from angle %3 to %4 with radius %5"
//                            .arg(origin.x).arg(origin.y).arg(startingAngle).arg(endingAngle).arg(radius)
//        console.log(message)
    context.beginPath()
    context.lineWidth = lineWidth
    context.strokeStyle = lineColor
    // For some reasons, the Canvas HTML standard uses clockwise direction from angles, against common mathematics operations
    // https://html.spec.whatwg.org/multipage/canvas.html#dom-context-2d-arc-dev
    context.arc(origin.x, origin.y, radius, 2 * Math.PI - startingAngle, 2 * Math.PI - endingAngle, true)
//    console.log("context.arc(%1, %2, %3, %4, %5)".arg(origin.x).arg(origin.y).arg(radius).arg(startingAngle).arg(endingAngle))
    context.stroke()
    context.closePath()
}

function drawLine(context, start, end, lineColor = "black", lineWidth = 2) {
//        var message = "Drawing line from (%1, %2) to (%3, %4)".arg(start.x).arg(start.y).arg(end.x).arg(end.y)
//        console.log(message)

    context.beginPath()
    context.lineWidth = lineWidth
    context.strokeStyle = lineColor
    context.moveTo(start.x, start.y)
    context.lineTo(end.x, end.y)
    context.stroke()
    context.closePath()
}

function drawPath(context, aPath, color = "blue", lineThickness = 1) {
    context.save()
    context.strokeStyle = color
    context.path = aPath
    context.lineWidth = lineThickness
    context.stroke()
}

function getAngle(origin, point) {
    var length = point.x - origin.x;
    var height = origin.y - point.y;
//    print("Length, height: %1, %2".arg(length).arg(height))
    var radius = Math.sqrt(length * length + height * height)
//    console.log("Radius: %1".arg(radius))
    var theta = Math.atan(height / length)
    if (length < 0) return theta + Math.PI
    else if (length > 0 && height < 0) return theta + 2*Math.PI
    else return theta
}

function getPointFromAngle(origin, radius, angle) {
    var x = Math.round(origin.x + radius * Math.cos(angle))
    var y = Math.round(origin.y - radius * Math.sin(angle))
    return Qt.point(x, y)
}

function getRandomIndex(arr) {
    var index = Math.floor(Math.random()*arr.length)
    if (index >= arr.length)
        index = arr.length - 1
    else if (index < 0)
        index = 0
    return index
}

// from MDN
function getRandomInRange(min, max) {
  return Math.random() * (max - min) + min;
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min +1)) + min;
}
