import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt5Compat.GraphicalEffects

Item {
    width: 320
    height: 240
    property alias colorOverlay: colorOverlay

    GridLayout {
        id: gridLayout
        anchors.fill: parent

        Item {
            id: imageClipper
            width: 71
            height: 71
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            clip: true
            Image {
                id: image
                source: "../../assets/animals/rat-charset.png"
                clip: true
                fillMode: Image.PreserveAspectFit
                ColorOverlay {
                    id: colorOverlay
                    anchors.fill: parent
                    source: parent
                    opacity: 0.3
                }
            }
        }


        GridLayout {
            id: gridLayout1
            width: 100
            height: 100
            columns: 2

            Label {
                id: nameLabel
                text: qsTr("Name")
                Layout.columnSpan: 2
            }

            Label {
                id: healthLabel
                text: qsTr("Health")
                Layout.columnSpan: 2
            }

            Label {
                id: psyLabel
                text: qsTr("Psychological State")
                Layout.columnSpan: 2
            }

            Label {
                id: behaviourLabel
                text: qsTr("Behaviour")
            }

            ComboBox {
                id: behaviourComboBox
            }

            Label {
                id: genderLabel
                text: qsTr("Gender")
            }

            ComboBox {
                id: genderComboBox
            }
        }


    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
