import QtQuick

import my.mazes
import "../../utils.js" as Utils
import "../.."

Rectangle {
    signal mouseReady(var mouse)
    signal pathReady(int mouseId, Path path, var cell)

    color: "transparent"

    property int mouseCounter: 0
    property var miceList: ({})
    property var foodList: ({})
    property bool shouldShowGraph: false

    Component {
        id: graphComponent
        Circular {}
    }

    Component {
        id: pathComponent
        Path {

        }
    }

    Component {
        id: pathArcComponent
        PathArc {}
    }

    Component {
        id: pathLineComponent
        PathLine {}
    }

    Component {
        id: mouseComponent
        Mouse {
            id: mouseItem
        }

    }

    Component {
        id: foodComponent
        FoodItem {
            id: foodItem
        }
    }

    // Add elements
    function addMouse(mouseSettings) {
        var cell = maze.graph.getRandomCell()
        var point = Utils.getPointFromAngle(maze.center, cell.level * maze.cellSize, cell.angle)
        mouseCounter += 1
        var size;
        if (mouseSettings.age == Mouse.Age.Pup) size = maze.cellSize / 2
        else size = 2* maze.cellSize / 3
        mouseSettings["identifier"] = mouseCounter
        mouseSettings["currentCell"] = cell
        mouseSettings["previousCell"] = cell
        mouseSettings["useMoveTransitions"] = false
        mouseSettings["width"] = size
        mouseSettings["height"] = size
        var mouse = mouseComponent.createObject(maze, mouseSettings)

        miceList[mouseCounter] = mouse
        mouse.x = point.x - mouse.width / 2
        mouse.y = point.y - mouse.height / 2
        mouse.picture.frameX = 0
        mouse.requestPath.connect(getPath)
        pathReady.connect(mouse.onPathReceived)
        mouse.ateFoodAt.connect(removeFood)
        mouseReady(mouse)
    }

    function removeMouse(mouseId) {
        miceList[mouseId].destroy()
        delete miceList[mouseId]
    }

    function addFood(restoreValue) {
        console.log("Adding food")
        if (foodList.length === maze.graph.cells.length) {
            console.log("Cannot add food. Reason: all cells have food")
            return
        }
        var cell = maze.graph.getRandomCell()
        while (cell.id in foodList) {
            cell = maze.graph.getRandomCell()
        }

        console.log("to cell %1".arg(cell.id))

        var point = getCellCenter(cell)
        var foodItem = foodComponent.createObject(maze, {
                                                      cellId: cell.id,
                                                      restoreValue: restoreValue,
                                                      width: maze.cellSize / 3,
                                                      height: maze.cellSize / 3
                                                  })
        foodItem.x = point.x - foodItem.width / 2
        foodItem.y = point.y - foodItem.height / 2
        foodList[cell.id] = foodItem

        console.log("Food added at %1".arg(point))
    }

    function removeFood(cellId) {
        foodList[cellId].destroy()
        delete foodList[cellId]
    }


    function onMouseActionReceived(action) {
        console.log("Maze::mouseActionReceived -> %1".arg(action))
        var actionDetails = action.split(";")
        var mouseId = parseInt(actionDetails.shift())
        miceList[mouseId].appendAction(actionDetails)
        if (!miceList[mouseId].started)
            miceList[mouseId].performNextAction()
    }

    function onGraphReceived(jsonObject) {
        maze.graph = graphComponent.createObject()
        maze.graph.load(jsonObject)
        repaint()
    }

    function repaint() {
        if (maze.graph != null && maze.graph.cells.length !== 0) {
            maze.cellSize = maze.width / (2 * maze.graph.radius)
            maze.context.reset()
            maze.requestPaint()
        }

    }

    function clear() {
        if (maze.graph != null)
            maze.graph.destroy()
        for (var f in foodList) {
            removeFood(f)
        }

        for (var m in miceList) {
            removeMouse(m)
        }
        maze.context.reset()
        maze.requestPaint()
    }

    function showGraph(shouldShow) {
        shouldShowGraph = shouldShow
        repaint()
    }

    function getCellCenter(cell) {
        var radius = cell.level * maze.cellSize
        return Utils.getPointFromAngle(maze.center, radius, cell.angle)
    }

    function drawPath(srcId, targetId, color = "blue", lineThickness = 1) {
        var cellList = maze.graph.cells
        var srcCell = cellList[srcId]
        var targetCell = cellList[targetId]
        var path = makePath(srcCell, targetCell)
        maze.context.save()
        maze.context.strokeStyle = color
        maze.context.path = path
        maze.context.lineWidth = lineThickness
        maze.context.stroke()
    }

    function makePath(srcCell, targetCell) {
        var srcPoint = getCellCenter(srcCell)
        var targetPoint = getCellCenter(targetCell)
        var movePath = pathComponent.createObject(maze)
        movePath.startX = srcPoint.x
        movePath.startY = srcPoint.y
        var srcRadius = srcCell.level * maze.cellSize
        var targetRadius = targetCell.level * maze.cellSize
        var sameLevel = srcCell.level === targetCell.level

        if (sameLevel) {
            var pathArc = pathArcComponent.createObject(movePath)
            pathArc.x = targetPoint.x
            pathArc.y = targetPoint.y
            pathArc.radiusX = srcRadius
            pathArc.radiusY = srcRadius

            var isClockwise = srcCell.index > targetCell.index && targetCell.index !== 0
                    || srcCell.index === 0 && targetCell.angle > 3 * Math.PI / 2
                    || targetCell.index === 0 && srcCell.angle < Math.PI / 2

            if (!isClockwise)
                pathArc.direction = PathArc.Counterclockwise
            else
                pathArc.direction = PathArc.Clockwise
            movePath.pathElements.push(pathArc)
        } else {
            if (srcCell.angle === targetCell.angle || srcCell === maze.graph.cells[0] || targetCell === maze.graph.cells[0]) {
                var pathLine = pathLineComponent.createObject(movePath)
                pathLine.x = targetPoint.x
                pathLine.y = targetPoint.y
                movePath.pathElements.push(pathLine)
            } else {
                var srcAngleSize = 2 * Math.PI / maze.graph.numCellsForLevel(srcCell.level)
                var targetAngleSize = 2 * Math.PI / maze.graph.numCellsForLevel(targetCell.level)
                var srcCellOffset = 0
                var targetCellOffset = 0
//                var isClockwise = !(srcCell.angle < Math.PI / 2 && targetCell.angle > 3 * Math.PI / 2) && (targetCell.angle < srcCell.angle)
                var isClockwise = (srcCell.index > targetCell.index && targetCell.index !== 0)
                        || (srcCell.index === 0 && targetCell.angle > 3 * Math.PI / 2)
                        || (targetCell.index === 0 && srcCell.angle < Math.PI / 2)
//                print("Is clockwise (%1 - %2): %3
//srcCell.index > targetCell.index && targetCell.index !== 0: %4
//srcCell.index === 0 && targetCell.angle > 3 * Math.PI / 2: %5
//targetCell.index === 0 && srcCell.angle > Math.PI / 2: %6"
//                      .arg(srcCell.id)
//                      .arg(targetCell.id)
//                      .arg(isClockwise)
//                      .arg(srcCell.index > targetCell.index && targetCell.index !== 0)
//                      .arg(srcCell.index === 0 && targetCell.angle > 3 * Math.PI / 2)
//                      .arg(targetCell.index === 0 && srcCell.angle < Math.PI / 2))
                var isTowardsInner = srcCell.level > targetCell.level
                var srcAngleOffset = 0
                var targetAngleOffset = 0
                if (isTowardsInner) {
                    srcAngleOffset = srcAngleSize / 4
                    targetAngleOffset = targetAngleSize / 3
                } else {
                    srcAngleOffset = srcAngleSize / 3
                    targetAngleOffset = targetAngleSize / 4
                }

                var arcSectionStart = pathArcComponent.createObject(movePath)
                var lineSectionToLevel = pathLineComponent.createObject(movePath)
                var arcSectionEnd = pathArcComponent.createObject(movePath)
                // srcIndex == 0, targetIdex == max index for level
                if (srcCell.index === 0 && targetCell.angle > 3 * Math.PI / 2) {
                    srcCellOffset = Utils.getPointFromAngle(maze.center, srcRadius, 2 * Math.PI - srcAngleOffset);
                    targetCellOffset = Utils.getPointFromAngle(maze.center, targetRadius, targetCell.angle + targetAngleOffset)
                // srcIndex == max index for level, targetIndex == 0
                } else if (targetCell.index === 0 && srcCell.angle > 3 * Math.PI / 2) {
                    srcCellOffset = Utils.getPointFromAngle(maze.center, srcRadius, srcCell.angle + srcAngleOffset);
                    targetCellOffset = Utils.getPointFromAngle(maze.center, targetRadius, 2 * Math.PI - targetAngleOffset)
                } else if (srcCell.angle > targetCell.angle) {
                    srcCellOffset = Utils.getPointFromAngle(maze.center, srcRadius, srcCell.angle - srcAngleOffset);
                    targetCellOffset = Utils.getPointFromAngle(maze.center, targetRadius, targetCell.angle + targetAngleOffset)
                }  else {
                    srcCellOffset = Utils.getPointFromAngle(maze.center, srcRadius, srcCell.angle + srcAngleOffset);
                    targetCellOffset = Utils.getPointFromAngle(maze.center, targetRadius, targetCell.angle - targetAngleOffset)
                }
                arcSectionStart.x = srcCellOffset.x
                arcSectionStart.y = srcCellOffset.y
                arcSectionStart.radiusX = srcPoint.x
                arcSectionStart.radiusY = srcPoint.y
                lineSectionToLevel.x = targetCellOffset.x
                lineSectionToLevel.y = targetCellOffset.y
                arcSectionEnd.x = targetPoint.x
                arcSectionEnd.y = targetPoint.y
                arcSectionEnd.radiusX = targetPoint.x
                arcSectionEnd.radiusY = targetPoint.y

                if (isClockwise) {
                    arcSectionStart.direction = PathArc.Clockwise
                    arcSectionEnd.direction = PathArc.Clockwise
                } else {
                    arcSectionStart.direction = PathArc.Counterclockwise
                    arcSectionEnd.direction = PathArc.Counterclockwise
                }
                movePath.pathElements.push(arcSectionStart)
                movePath.pathElements.push(lineSectionToLevel)
                movePath.pathElements.push(arcSectionEnd)
            }
        }
        return movePath
    }

    function getPath(mouseId, currentCellId, nextCellId) {
        var currentCell = maze.graph.cells[currentCellId]
        var nextCell = maze.graph.cells[nextCellId]

        var path = makePath(currentCell, nextCell)
        pathReady(mouseId, path, nextCell)
    }

    function drawGraph() {
        maze.context.save()
        var cellList = maze.graph.cells;
        Utils.drawDot(maze.context, maze.center)
        Utils.drawText(maze.context, maze.center, cellList[0].index)

        for (var i = 1; i < cellList.length; ++i) {
            var cell = cellList[i]

            var neighbours = maze.graph.neighboursFor(cell)
//            console.log("Cell %1 (neigbours):".arg(cell.id))
//            for (var n = 0; n < neighbours.length; ++n) {
//                var neigh = neighbours[n]
////                console.log("%1".arg(neigh.id))
//                if (maze.graph.hasConnectionBetween(cell, neigh)) {
//                    var aPath = pathList[cell][neigh]
////                    console.log("path: %1".arg(aPath))
////                    drawAPath(aPath, "green", 4)
//                    drawPath(cell.id, neigh.id, "green", 4)
//                }
//            }

            var radius = cell.level * maze.cellSize
            var angle = cell.angle
            var cellCenterPoint = Utils.getPointFromAngle(maze.center, radius, angle)

            // Check and connect if needed the neighbour couter clockwise
            var ccw = maze.graph.ccwCellFor(cell)
            if (cell !== null && ccw !== null) {
                var connectedToCcw = maze.graph.areConnected(cell, ccw)
                if (connectedToCcw) {
//                    Utils.drawArc(maze.context, maze.center, cell.angle, ccw.angle, radius, "red")
                    drawPath(cell.id, ccw.id)
                }
            }

            // Check and connect if needed the inward cells
            var inwardCells = maze.graph.inwardCellsFor(cell)
            var inwardRadius = radius - maze.cellSize
            var angleSize = 2 * Math.PI / maze.graph.numCellsForLevel(cell.level)
            var inwardAngleSize = 2 * Math.PI / maze.graph.numCellsForLevel(cell.level - 1)
            var outwardCells = maze.graph.outwardCellsFor(cell)


            for (var c = 0; c < inwardCells.length; ++c) {
                var inCell = inwardCells[c]
                if (maze.graph.areConnected(cell, inCell)) {
                    if (cell.angle !== inCell.angle && inCell.level !== 0) {
                        var currCellOffset = 0
                        var inwardOffset = 0
                        if (cell.angle > inCell.angle) {
//                            Utils.drawArc(maze.context, maze.center, cell.angle - angleSize / 4, cell.angle, radius, "red")
//                            Utils.drawArc(maze.context, maze.center, inCell.angle, inCell.angle + inwardAngleSize / 3, inwardRadius, "red")
                            currCellOffset = Utils.getPointFromAngle(maze.center, radius, cell.angle - angleSize /4);
                            inwardOffset = Utils.getPointFromAngle(maze.center, inwardRadius, inCell.angle + inwardAngleSize /3)
//                            Utils.drawLine(maze.context, currCellOffset, inwardOffset, "red")
                        }  else {
//                            Utils.drawArc(maze.context, maze.center, cell.angle, cell.angle + angleSize / 4, radius, "red")
//                            Utils.drawArc(maze.context, maze.center, inCell.angle - inwardAngleSize / 3, inCell.angle, inwardRadius, "red")
                            currCellOffset = Utils.getPointFromAngle(maze.center, radius, cell.angle + angleSize /4);
                            inwardOffset = Utils.getPointFromAngle(maze.center, inwardRadius, inCell.angle - inwardAngleSize /3)
                            }

//                        Utils.drawLine(maze.context, currCellOffset, inwardOffset, "red")
//                        drawPath(cell.id, inCell.id)

                    } else {
                        var start = Utils.getPointFromAngle(maze.center, inCell.level * maze.cellSize, inCell.angle);
                        var end = Utils.getPointFromAngle(maze.center, radius, cell.angle)
//                        Utils.drawLine(maze.context, start, end, "red")
                    }

                    var inCellPoint = Utils.getPointFromAngle(maze.center, inwardRadius, inCell.angle)
                    var midX = (cellCenterPoint.x + inCellPoint.x) / 2
                    var midY = (cellCenterPoint.y + inCellPoint.y) / 2
//                    Utils.drawText(maze.context, Qt.point(midX, midY), "%1 - %2".arg(cell.id).arg(inCell.id), "blue")
                    drawPath(cell.id, inCell.id)
                }
            }

            Utils.drawDot(maze.context, cellCenterPoint)
            Utils.drawText(maze.context, cellCenterPoint, cell.id)
            Utils.drawText(maze.context, Qt.point(cellCenterPoint.x, cellCenterPoint.y + 20), cell.index, "red")
        }
    }

    function drawMaze() {
        maze.context.save()
        var cellList = maze.graph.cells
        for (var i = 1; i < cellList.length; ++i) {
            var cell = cellList[i]
            var radius = cell.level * maze.cellSize
            var angleSize = 2 * Math.PI / maze.graph.numCellsForLevel(cell.level)
            var innerRadius = cell.level * maze.cellSize - maze.cellSize /2 // inner wall
            var outerRadius = cell.level * maze.cellSize + maze.cellSize /2 // outer wall

            var theta_cw = (cell.angle - angleSize / 2)
            var theta_ccw = (cell.angle + angleSize / 2)

            var a = Utils.getPointFromAngle(maze.center, innerRadius, theta_cw)
            var b = Utils.getPointFromAngle(maze.center, outerRadius, theta_cw)
            var c = Utils.getPointFromAngle(maze.center, innerRadius, theta_ccw)
            var d = Utils.getPointFromAngle(maze.center, outerRadius, theta_ccw)

            var ccwCell = maze.graph.ccwCellFor(cell)

            if (!maze.graph.areConnected(cell, ccwCell)) {
                Utils.drawLine(maze.context, c, d, "black", 3)
            }

            var inwardCells = maze.graph.inwardCellsFor(cell)
            var inwardRadius = radius - maze.cellSize

            if (inwardCells.length === 1 && !maze.graph.areConnected(cell, inwardCells[0])) {
                Utils.drawArc(maze.context, maze.center, theta_cw, theta_ccw, innerRadius, "black", 3)
                var anotherPoint = Utils.getPointFromAngle(maze.center, innerRadius, cell.angle)
            } else {
                for (var j = 0; j < inwardCells.length; ++j) {
                    var inwardCell = inwardCells[j]
                    if (!maze.graph.areConnected(cell, inwardCell)) {
                        var inwardIsCw = !(inwardCell.angle < Math.PI / 2 && cell.angle > 3 * Math.PI / 2) && (inwardCell.angle < cell.angle)

                        if (inwardIsCw) {
                            Utils.drawArc(maze.context, maze.center, theta_cw, cell.angle, innerRadius, "black", 3)
                        } else {
                            Utils.drawArc(maze.context, maze.center, cell.angle, theta_ccw, innerRadius, "black", 3)
                        }
                        var aPoint = Utils.getPointFromAngle(maze.center, innerRadius, cell.angle)
                    }
                }
            }
        }

        Utils.drawCircle(maze.context, maze.center, (maze.graph.radius - 1) * maze.cellSize + maze.cellSize / 2, "black", 3)

    }

    Canvas {
        id: maze
        width: Math.min(parent.height, parent.width)
        height: width
        anchors.fill: parent

        property int centerX: width / 2
        property int centerY: width / 2

        property var context

        property Circular graph

        property point center: Qt.point(width / 2, height / 2)
        property int cellSize

        onAvailableChanged: {
            if (available)
                context = getContext("2d")
        }

//        Component.onCompleted: console.log("Maze created! Center: %1, %2".arg(center.x).arg(center.y))

        onPaint: {
            if (graph != null) {
//                context = getContext("2d")
                if (shouldShowGraph) {
                    drawGraph()
                }
                drawMaze()
            }
        }
    }

    function toAdjacencyList() {
        return maze.graph.toAdjacencyList();
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
