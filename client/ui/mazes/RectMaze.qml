import QtQuick

import my.mazes
import "../../utils.js" as Utils
import "../.."

Rectangle {
    signal mouseReady(var mouse)
    signal pathReady(int mouseId, Path path, var cell)

    color: "transparent"

    property int mouseCounter: 0
    property var miceList: ({})
    property var foodList: ({})
    property bool shouldShowGraph: false

    Component {
        id: graphComponent
        Rectangular {}
    }

    Component {
        id: pathComponent
        Path {

        }
    }

    Component {
        id: pathLineComponent
        PathLine {}
    }

    Component {
        id: mouseComponent
        Mouse {
            id: mouseItem
        }

    }

    Component {
        id: foodComponent
        FoodItem {
            id: foodItem
        }
    }


    // Add elements
    function addMouse(mouseSettings) {
        console.log("Adding mouse")
        var cell = maze.graph.getRandomCell()
        var point = getPoint(cell.xPos, cell.yPos)
        mouseCounter += 1
        var size;
        if (mouseSettings.age == Mouse.Age.Pup) size = maze.cellSize / 3
        else size = 2* maze.cellSize / 3
        mouseSettings["identifier"] = mouseCounter
        mouseSettings["currentCell"] = cell
        mouseSettings["previousCell"] = cell
        mouseSettings["useMoveTransitions"] = true
        mouseSettings["width"] = size
        mouseSettings["height"] = size
        var mouse = mouseComponent.createObject(maze, mouseSettings)
        console.log(JSON.stringify(mouseSettings))
        miceList[mouseCounter] = mouse
        mouse.x = point.x - mouse.width / 2
        mouse.y = point.y - mouse.height / 2
        mouse.picture.frameX = 0
        mouse.requestPath.connect(getPath)
        pathReady.connect(mouse.onPathReceived)
        mouse.ateFoodAt.connect(removeFood)
        mouseReady(mouse)
    }

    function removeMouse(mouseId) {
        miceList[mouseId].destroy()
        delete miceList[mouseId]
    }

    function addFood(restoreValue) {

        if (foodList.length === maze.graph.cells.length)
            return
        var cell = maze.graph.getRandomCell()
        while (cell.id in foodList) {
            cell = maze.graph.getRandomCell()
        }

        var point = getPoint(cell.xPos, cell.yPos)
        var foodItem = foodComponent.createObject(maze, {
                                                      cellId: cell.id,
                                                      restoreValue: restoreValue,
                                                      width: maze.cellSize / 3,
                                                      height: maze.cellSize / 3
                                                  })
        foodItem.x = point.x - foodItem.width / 2
        foodItem.y = point.y - foodItem.height / 2
        foodList[cell.id] = foodItem
    }

    function removeFood(cellId) {
        foodList[cellId].destroy()
        delete foodList[cellId]
    }

    function onMouseActionReceived(action) {
        console.log("Maze::mouseActionReceived -> %1".arg(action))
        var actionDetails = action.split(";")
        var mouseId = parseInt(actionDetails.shift())
        miceList[mouseId].appendAction(actionDetails)
        if (!miceList[mouseId].started)
            miceList[mouseId].performNextAction()
    }

    function onGraphReceived(jsonObject) {
        maze.graph = graphComponent.createObject()
        maze.graph.load(jsonObject)
        repaint()
    }

    function repaint() {
        if (maze.graph != null && maze.graph.cells.length !== 0) {
            var cellWidth = maze.width / maze.graph.width
            var cellHeight = maze.height / maze.graph.height
            console.log("graph width: %1".arg(maze.graph.width))
            console.log("graph height: %1".arg(maze.graph.height))
            if (cellWidth < cellHeight) {
                maze.cellSize = cellWidth
            } else {
                maze.cellSize = cellHeight
            }
            maze.context.reset()
            maze.requestPaint()
            console.log(maze.cellSize)
        }
    }

    function clear() {
        if (maze.graph)
            maze.graph.destroy()
        for (var f in foodList) {
            removeFood(f)
        }

        for (var m in miceList) {
            removeMouse(m)
        }

        maze.context.reset()
        maze.requestPaint()
    }

    function showGraph(shouldShow) {
        shouldShowGraph = shouldShow
        repaint()
    }

    function cellToString(cell) {
        if (cell !== null) {
            return "%1 (%2, %3)".arg(cell.id).arg(cell.xPos).arg(cell.yPos)
        } else {
            return "NULL"
        }
    }

    function toAdjacencyList() {
        return maze.graph.toAdjacencyList()
    }

    function getPoint(x, y) {
        var xPos = Math.floor(x * maze.cellSize + maze.cellSize / 2)
        var yPos = Math.floor(y * maze.cellSize + maze.cellSize / 2)
        return Qt.point(xPos, yPos)
    }

    function getCell(x, y) {
        return maze.graph.cells[x + maze.graph.width * y]
    }

    function drawGraph() {
        for (var x = 0; x < maze.graph.width; ++x ) {

            for (var y = 0; y < maze.graph.height; ++y) {
                var point = getPoint(x, y)
                var cell = getCell(x, y)
                Utils.drawDot(maze.context, point)
                Utils.drawText(maze.context, point, "(%1,%2)"
                               .arg(x).arg(y))
                Utils.drawText(maze.context, Qt.point(point.x, point.y + 20),
                               cell.id, "red")

                var connections = maze.graph.getConnections(cell);
                console.log("Cell %1 (%2, %3)".arg(cell.id).arg(cell.xPos).arg(cell.yPos))
                for (var i = 0; i < connections.length; ++ i) {
                    var n = connections[i]
                    console.log("\tn %1 (%2, %3)".arg(n.id).arg(n.xPos).arg(n.yPos))
                    var path = makePath(cell, n)
                    Utils.drawPath(maze.context, path)
                }
            }
        }
    }

    function drawMaze() {
        for (var x = 0; x < maze.graph.width; ++x ) {
            for (var y = 0; y < maze.graph.height; ++y) {
                var cell = getCell(x, y);
//                console.log("currCell " + cellToString(cell))
                var pt = getPoint(cell.xPos, cell.yPos)
                var topLeftX = Math.max(pt.x - maze.cellSize / 2, 0);
                var topLeftY = Math.max(pt.y - maze.cellSize / 2, 0);

                var bottomLeftX = Math.max(pt.x - maze.cellSize / 2, 0);
                var bottomLeftY = Math.min(pt.y + maze.cellSize / 2, maze.height);

                var topRightX = Math.min(pt.x + maze.cellSize / 2, maze.width);
                var topRightY = Math.max(pt.y - maze.cellSize / 2, 0);

                var bottomRightX = Math.min(pt.x + maze.cellSize / 2, maze.width);
                var bottomRightY = Math.min(pt.y + maze.cellSize / 2, maze.height);


                var topLeft = Qt.point(topLeftX, topLeftY)
                var bottomLeft = Qt.point(bottomLeftX, bottomLeftY)
                var topRight = Qt.point(topRightX, topRightY)
                var bottomRight = Qt.point(bottomRightX, bottomRightY)

                var leftCell = maze.graph.leftNeighbour(cell);
//                console.log("\tleft " + cellToString(leftCell))


                var rightCell = maze.graph.rightNeighbour(cell);
//                console.log("\tright " + cellToString(rightCell))

                var upCell = maze.graph.upNeighbour(cell);
//                console.log("\tup " + cellToString(upCell))

                var downCell = maze.graph.downNeighbour(cell);
//                console.log("\tdown " + cellToString(downCell))

                // "black", 3
                if (leftCell === null || !maze.graph.areConnected(cell, leftCell)) {
                    Utils.drawLine(maze.context, topLeft, bottomLeft, "black", 3)
                }

                if (rightCell === null || !maze.graph.areConnected(cell, rightCell)) {
                    Utils.drawLine(maze.context, topRight, bottomRight, "black", 3)
                }

                if (upCell === null || !maze.graph.areConnected(cell, upCell)) {
                    Utils.drawLine(maze.context, topLeft, topRight, "black", 3)
                }

                if (downCell === null || !maze.graph.areConnected(cell, downCell)) {
                    Utils.drawLine(maze.context, bottomLeft, bottomRight, "black", 3)
                }
            }
        }

    }

    function makePath(srcCell, targetCell) {
        var srcPoint = getPoint(srcCell.xPos, srcCell.yPos)
        var targetPoint = getPoint(targetCell.xPos, targetCell.yPos)

        var movePath = pathComponent.createObject(maze)
        movePath.startX = srcPoint.x
        movePath.startY = srcPoint.y
        var pathLine = pathLineComponent.createObject(movePath)
        pathLine.x = targetPoint.x
        pathLine.y = targetPoint.y
        movePath.pathElements.push(pathLine)
        return movePath
    }

    function getPath(mouseId, currentCellId, nextCellId) {
        var currentCell = maze.graph.cells[currentCellId]
        var nextCell = maze.graph.cells[nextCellId]

        var path = makePath(currentCell, nextCell)
        pathReady(mouseId, path, nextCell)
    }

    Canvas {
        id: maze
        width: Math.min(parent.height, parent.width)
        height: width
        anchors.fill: parent

        property var context

        property Rectangular graph

        property point center: Qt.point(width / 2, height / 2)
        property int cellSize

        onAvailableChanged: {
            if (available)
                context = getContext("2d")
        }

//        Component.onCompleted: console.log("Maze created! Center: %1, %2".arg(center.x).arg(center.y))

        onPaint: {
            if (graph != null) {
//                context = getContext("2d")
                if (shouldShowGraph) {
                    drawGraph()
                }
                drawMaze()
            }
        }
    }
}
