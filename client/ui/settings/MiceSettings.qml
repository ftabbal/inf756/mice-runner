import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt5Compat.GraphicalEffects

import "../.."

GroupBox {

    title: qsTr("Mouse")

    signal addMouse(var mouseSettings)

    GridLayout {
        anchors.fill: parent
        columns: 4

        Label {
            id: healthLabel
            text: qsTr("Health")
            Layout.leftMargin: 5
        }

        SpinBox {
            id: defaultHealthPoints
            from: 30
            to: 100
            value: 40
        }

        Label {
            id: mentalPointsLabel
            text: qsTr("Mental")
        }

        SpinBox {
            id: defaultMentalPoints
            from: 30
            to: 100
            value: 40
        }

        Label {
            Layout.leftMargin: 5
            id: sexLabel
            text: qsTr("Sex")
        }

        ComboBox {
            id: sexComboBox
            model: ["Random", "Female", "Male"]
            currentIndex: 0
        }

        Label {
            id: ageLabel
            text: qsTr("Age")
        }

        ComboBox {
            id: ageComboBox
            model: ["Random", "Pup", "Prime", "Old"]
        }

        Label {
            id: placeholder
        }

        Button {
            id: addMouseButton
            text: qsTr("Add mouse")
            onClicked: {
                var sex = getSex()
                var age = getAge()
                var settings = {
                    health: defaultHealthPoints.value,
                    psy: defaultMentalPoints.value,
                    sex: sex,
                    age: age
                }

                addMouse(settings)
            }
        }
    }

    function getSex() {
        var index = sexComboBox.currentIndex
        if (index === 0)
            index = Math.floor(Math.random() * 2 + 1)
        switch (index) {
        case 1: return Mouse.Sex.Female
        case 2: return Mouse.Sex.Male
        default: getSex()
        }
    }

    function getAge() {
        var index = ageComboBox.currentIndex
        if (index === 0)
            index = Math.floor(Math.random() * 3 + 1)
        switch (index) {
        case 1: return Mouse.Age.Pup
        case 2: return Mouse.Age.Prime
        case 3: return Mouse.Age.Old
        default: getSex()
        }
    }




    //    function addMouse(mouse) {
    //        //        console.log("Mouse received! id: %1 (%2, %3)".arg(mouse.id).arg(mouse.currentCell.id))
    //        console.log("Mouse received!")
    //        var mouseItem = mouseOverview.createObject()
    //        //        mouseItem.picture = mouse.picture
    //        //        mouseItem.picture.colorOverlay.color = mouse.picture.colorOverlay.color
    //        listModel.append(mouseItem)
    //    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
