import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import "../../api/setup_service.js" as Service
import "maze"

GroupBox {
    signal showGraphChanged(bool show)
    signal clearMaze()
    signal shapeSelected(string shape)
    signal graphReceived(var graph)

    title: qsTr("Maze")

    Component.onCompleted: {
        shapeSettings.shapeSelected.connect(shapeSelected)
    }

    GridLayout {
        id: gridLayout
//        anchors.fill: parent
        columns: 3

        Label {
            text: qsTr("Shape")
            Layout.leftMargin: 5
        }

        ShapeSettings {
            id: shapeSettings
            Layout.columnSpan: 2
        }

        Label {
            text: qsTr("Algorithm")
            Layout.leftMargin: 5
            Layout.rowSpan: 2
        }

        AlgorithmSettings {
            id: algoSettings
            Layout.columnSpan: 2
            Layout.rowSpan: 2
        }

        Label {
            Layout.leftMargin: 5
            text: qsTr("Options")
        }

        CheckBox {
            Layout.leftMargin: 2
            Layout.minimumWidth: 140
            id: braidCheckBox
            text: qsTr("Braid maze")

        }


        CheckBox {
            id: showGraphCheckBox
            onCheckedChanged: showGraphChanged(checked)
            text: qsTr("Show graph")
            enabled: graphReady
        }

        Label {
            id: placeholder
        }

        RowLayout {
            Layout.columnSpan: 2
            Button {
                Layout.preferredWidth: 120
                id: generateMazeButton
                text: qsTr("Generate maze")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                enabled: shapeSettings.ready && algoSettings.ready
                onClicked: {
                    var shape = shapeSettings.shape
                    var size = shapeSettings.shapeSize
                    var algo = algoSettings.algo
                    Service.getMaze(shape, size, algo, function(response){
                        console.log("Response received!")
                        console.log(JSON.stringify(response))
                        graphReceived(response)
                        graphReady = true
                    })
                }
            }

            Button {
                Layout.preferredWidth: 120
                id: resetMazeButton
                text: qsTr("Reset maze")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                onClicked: reset()
                enabled: shapeSettings.ready || algoSettings.ready
            }
        }


    }

    function reset() {
        shapeSettings.reset()
        algoSettings.reset()
        clearMaze()
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
