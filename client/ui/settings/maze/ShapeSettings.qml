import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {
//    title: qsTr("Maze")

    signal shapeSelected(string shape)

    property string shape
    property string shapeSize: sizeSettings.sizeString

    property bool ready: shape !== "" && shapeSize !== ""

    height: 35

    RowLayout {
        id: columnLayout
        anchors.fill: parent

        ComboBox {
            id: shapeSelector
            flat: true
            Layout.minimumWidth: 140
            currentIndex: -1
            model: [qsTr("Circle"), qsTr("Rectangle")]
            onCurrentIndexChanged: {
                if (currentIndex === 0) {
                    sizeSettings.showCircleSize()
                    shapeSelected("Circle")
                    shape = "Circle"
                } else if (currentIndex === 1) {
                    sizeSettings.showDefaultSize()
                    shapeSelected("Rectangle")
                    shape = "Rectangle"
                } else {
                    sizeSettings.showPlaceholder()
                    shapeSelected("")
                    shape = ""
                }
            }
        }

        SizeSettings {
            id: sizeSettings
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            Layout.fillWidth: true
            Layout.leftMargin: 8
        }
    }
    
    function reset() {
        shapeSelector.currentIndex = -1
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1}
}
##^##*/
