import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {
    property bool ready
    property string sizeString

    width: 200
    height: 30

    StackLayout {
    id: stackLayout
//    anchors.fill: parent
//    height: Math.max(placeholder.height, circleSize.height, defaultSize.height)

        Item {
            id: placeholder
            Layout.fillWidth: true
//            Label {
//                text: qsTr("Select a type to enter a size")
//            }
        }

        Item {
            id: circleSize
            RowLayout {
                id: circleSizelayout
                anchors.fill: parent

                Label {
                    text: qsTr("Radius")
                }

                SpinBox {
                    Layout.leftMargin: 5
                    Layout.preferredWidth: 70
                    id: radiusField
                    to: 15
                    editable: true
                    from: 5
                    value: 8
                    onValueChanged: setString()
                }
            }
        }

        Item {
            id: defaultSize
            RowLayout {
                Label {
                    text: qsTr("Width")
                }

                SpinBox {
                    Layout.preferredWidth: 70
                    id: widthField
                    to: 30
                    editable: true
                    from: 10
                    value: 10
                    onValueChanged: setString()

                }

                Label {
                    Layout.leftMargin: 5
                    text: qsTr("Height")
                }

                SpinBox {
                    Layout.preferredWidth: 70
                    id: heightField
                    to: 30
                    editable: true
                    from: 10
                    value: 10
                    onValueChanged: setString()
                }
            }
        }
    }

    function setString() {
        if (stackLayout.currentIndex === 1) {
            sizeString = radiusField.displayText
        } else if (stackLayout.currentIndex === 2) {
            if (widthField.text === "" || heightField.text === "") {
                sizeString = ""
            } else {
                sizeString = widthField.displayText + "," + heightField.displayText
            }
        } else {
            sizeString = ""
        }
    }

    function showCircleSize() {
        stackLayout.currentIndex = 1
        setString()
    }

    function showDefaultSize() {
        stackLayout.currentIndex = 2
        setString()
    }

    function showPlaceholder() {
        stackLayout.currentIndex = 0
    }
}


