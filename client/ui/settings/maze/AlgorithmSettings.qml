import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Item {

    signal algoSelected(string algo)
    property bool ready
    property string algo
    height: 35
    id: algorithmGroup
//    title: qsTr("Algorithm")
    RowLayout {
        id: columnLayout
        anchors.fill: parent

        ComboBox {
            id: algoSelector
            currentIndex: -1
            Layout.minimumWidth: 140
            model: algoModel
            textRole: "key"
            valueRole: "value"
            onCurrentIndexChanged: {
                if (currentIndex === -1) {
                    algoDesc.text = qsTr("Please select an algorithm.")
                    algo = ""
                    ready = false
                }
                else {
                    algo = algoModel.get(currentIndex)["value"]
                    var text = algoModel.get(currentIndex)["desc"]
                    algoDesc.text = text
                    ready = true
                }
            }
        }

        Label {
            id: algoDesc
            text: qsTr("Please select an algorithm.")
            Layout.fillWidth: true
            Layout.leftMargin: 5
//            horizontalAlignment: Text.AlignHCenter
            font.italic: true
        }

        ListModel {
            id: algoModel
            ListElement {key: qsTr("Grid"); value: "Grid"; desc: qsTr("No walls to see all possible passages.")}
//            ListElement {key: qsTr("Deep-first Search"); value: "DFS"; desc: qsTr("A maze with balanced paths.")}
            ListElement {key: qsTr("Binary Tree"); value: "BT"; desc: qsTr("A simple biased algorithm.")}
            ListElement {key: "Aldous-Broder"; value: "AldousBroder"; desc: qsTr("A simple, unbiased algorithm")}
        }
    }

    function reset() {
        algoSelector.currentIndex = -1
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
