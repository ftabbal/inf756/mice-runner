import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

GroupBox {

    title: qsTr("Food")

    signal addFood(int value)

    ColumnLayout {
        RowLayout {
            Layout.fillWidth: true

            Label {
                id: foodRestoreValue
                text: qsTr("Number of restored health points:")
                Layout.leftMargin: 5
            }

            SpinBox {
                Layout.leftMargin: 5
                id: healthRestore
                to: 10
                editable: true
                from: 3
                value: 3
            }

        }

        Button {
            id: addFoodButton
            text: qsTr("Add food")
            onClicked: {
                addFood(healthRestore.value)
            }
        }
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
