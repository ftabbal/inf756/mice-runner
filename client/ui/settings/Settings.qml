import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import "../../api/setup_service.js" as Service
import "maze"

Rectangle {
    id: item1
    //    signal circleSizeChanged(string size)

    signal startClicked()
    signal graphReceived(var graph)
    signal showGraphChanged(bool show)

    signal addMouse(var mouse)
    signal addFood(int foodRestoreValue)

    signal clearMaze()

    property bool graphReady :false

    signal mazeShapeChanged(string shape)
    width: 480
    height: 640

    Component.onCompleted: {
        mazeSettings.shapeSelected.connect(mazeShapeChanged)
        mazeSettings.showGraphChanged.connect(showGraphChanged)
        mazeSettings.clearMaze.connect(clearMaze)
        mazeSettings.graphReceived.connect(graphReceived)


        miceSettings.addMouse.connect(addMouse)
        foodSettings.addFood.connect(addFood)
    }

    ColumnLayout {
        id: column
        anchors.fill: parent

        MazeSettings {
            id: mazeSettings
            Layout.fillWidth: true
        }

        MiceSettings {
            id: miceSettings
            Layout.fillWidth: true
            enabled: graphReady
        }

        FoodSettings {
            id: foodSettings
            Layout.fillWidth: true
            enabled: graphReady
        }

        RowLayout {

            Button {
                id: generateButton
                text: qsTr("Start simulation!")
                enabled: graphReady
                onClicked: startClicked()

            }

            Button {
                text: qsTr("Clear selection")
                enabled: graphReady
                onClicked: {
                    mazeSettings.reset()
                    graphReady = false
                    clearMaze()
                }
            }

        }

        Item {
            id: spacer

            Layout.fillHeight: true
            Rectangle {
                color: "transparent"
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.0625}
}
##^##*/
