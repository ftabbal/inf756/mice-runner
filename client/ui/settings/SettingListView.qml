import QtQuick

//import "FoodSettings.qml" as FoodSettings
//import "MazeSettings.qml" as MazeSettings
//import "MiceSettings.qml" as MiceSettings
//import "RuleSettings.qml" as RuleSettings

Rectangle {
    FoodSettings {
        id: foodSettings
    }

    MazeSettings {
        id: mazeSettings
    }

    MiceSettings {
        id: miceSettings
    }

    RuleSettings {
        id: ruleSettings
    }




    ListModel {
        id: widgetModel
//        ListElement {name: qsTr("Maze"); value: mazeSettings}
//        ListElement {name: qsTr("Mice"); value: miceSettings}
//        ListElement {name: qsTr("Food"); value: foodSettings}
//        ListElement {name: qsTr("Rules"); value: ruleSettings}
    }

    ListView {
        model: widgetModel

        anchors.fill: parent
        section.property: "name"
    }

}
