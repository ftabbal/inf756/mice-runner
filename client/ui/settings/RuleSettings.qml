import QtQuick
import QtQuick.Controls

Item {

    signal startClicked()

    Button {
        id: startButton
        text: qsTr("Start simulation")
        onClicked: startClicked()
    }



}
