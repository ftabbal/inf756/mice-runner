#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLocale>
#include <QTranslator>
#include <QtQml/qqmlregistration.h>

#include "mazes/circular.h"
#include "mazes/rectangular.h"
#include "mazes/qmlrectcellwrapper.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

//    QTranslator translator;
//    const QStringList uiLanguages = QLocale::system().uiLanguages();
//    for (const QString &locale : uiLanguages) {
//        const QString baseName = "MazeRendererTest_" + QLocale(locale).name();
//        if (translator.load(":/i18n/" + baseName)) {
//            app.installTranslator(&translator);
//            break;
//        }
//    }

    QQmlApplicationEngine engine;
//    const QUrl url(u"qrc:/MazeRendererTest/main.qml"_qs);
//    const QUrl url(u"qrc:/main.qml"_qs);
    const QUrl url(u"qrc:/StartUp.qml"_qs);
    qmlRegisterType<Maze::Circular>("my.mazes", 1, 0, "Circular");
    qmlRegisterType<Maze::Polar::QmlCellWrapper>("my.mazes", 1, 0, "PolarCell");
    qmlRegisterType<Maze::Rectangular>("my.mazes", 1, 0, "Rectangular");
    qmlRegisterType<Maze::Rectangle::QmlCellWrapper>("my.mazes", 1, 0, "RectCell");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
