import QtQuick
import QtQml
import my.mazes
import Qt5Compat.GraphicalEffects

import "utils.js" as Utils

Rectangle {

    enum Sex {
        Male,
        Female
    }

    enum Age {
        Pup,
        Prime,
        Old
    }

    id: me

    signal requestPath(int mId, var srcCell, var targetCell)
    signal ateFoodAt(int cellId)

    property bool started: false


    property var currentCell
    property var previousCell

    property int identifier

    property color mouseColor: colorOverlay.color

    property string name
    property int health
    property int psy
    property var sex
    property var age

    property bool useMoveTransitions: false

    property string behaviour: qsTr("Random")

    property alias picture: pict

    property bool isEating: false

    property double speedFactor

    property var nextActions: []

    width: 71
    height: 71
    color: "transparent"

    Image {
        id: sprite
        visible: false
        source: "assets/animals/rat-charset-with-shadow.png"
    }

    Component.onCompleted : {
        setSpeedFactor()
    }

    AnimatedSprite {
        id: pict
        x: 0
        y: 7
        source: sprite.source
        frameCount: 4
        frameWidth: 71
        frameHeight: 58
        interpolate: false
//        anchors.centerIn: parent
        anchors.fill: parent
        frameY: 174
        ColorOverlay {
            id: colorOverlay
            color: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
            anchors.fill: parent
            source: parent
            opacity: 0.3
        }
    }


    PathAnimation {
        id: pathAnim
        target: me
        duration: 500
        anchorPoint: Qt.point(me.width / 2, me.height / 2)
        orientation: !useMoveTransitions ? PathAnimation.TopFirst : PathAnimation.Fixed

        onFinished: {
            performNextAction()

        }
    }

    Timer {
        id: timer
        // Adapted from https://stackoverflow.com/questions/4959975/generate-random-number-between-two-numbers-in-javascript
        interval: Math.floor(Math.random() * (1000 - 500 + 1)) + 500;
        running: false
        repeat: false
    }

    function toObject() {
        var res = {
            "id": identifier,
            "currentCell": currentCell.id,
            "previousCell": previousCell.id,
            "health": health,
            "psy": psy,
            "speedFactor": speedFactor
        }
        return res
    }

    function appendAction(actionDetails) {
        nextActions.push(actionDetails)
    }

    function performNextAction() {
        // Create a random delay to avoid having all mice seem syncrhonized.
        if (!started) {
            started = true
            console.log("Mouse #%1: delay = %2".arg(identifier).arg(timer.interval))
            timer.start()
        }

        var action = nextActions.shift()
        console.log("Mouse #%1: %2".arg(identifier).arg(action))
        var actionHeader = action[0]
        if (actionHeader === "dead") {
            console.log("Mouse #%1 is dead.".arg(identifier))
            state = "dead"
            return
        }

        var nextCellId = parseInt(action[0])
        if (isNaN(nextCellId)) {
            console.log("Mouse #%1: should do %2".arg(identifier).arg(actionHeader))
            if (action[0] === "ate") {
                ateFoodAt(action[1])
                performNextAction()
            }
        } else {
            requestPath(identifier, currentCell.id, nextCellId)
        }

    }

    states: [
        State {
            name: "goingDown"
            PropertyChanges {
                target: pict
                frameY: 0
            }
        },

        State {
            name: "goingLeft"
            PropertyChanges {
                target: pict
                frameY: 58
            }
        },

        State {
            name: "goingRight"
            PropertyChanges {
                target: pict
                frameY: 116
            }
        },

        State {
            name: "goingUp"
            PropertyChanges {
                target: pict
                frameY: 174
            }
        },

        State {
           name: "moving"
           PropertyChanges {
               target: pict
               paused: false
           }
        },

        State {
            name: "stopped"
            PropertyChanges {
                target: pict
                paused: true
            }
        },

        State {
          name: "eating"
          PropertyChanges {
              target: pict
              paused: true

          }
        },

        State {
            name: "dead"
            PropertyChanges {
                target: pict
                paused: true

            }
        }

    ]

    function onPathReceived(mId, path, targetCell) {
        if (mId === identifier) {
            var lastPath = path.pathElements[path.pathElements.length - 1]
            pathAnim.path = path
            var rawDistance = 0
            for (var i = 0; i < path.pathElements.length; ++i) {
                var nextPath = path.pathElements[i]
                if (i == 0) {
                    rawDistance += Math.sqrt(Math.pow(path.startX - nextPath.x, 2) + Math.pow(path.startY - nextPath.y, 2))
                } else {
                    var prevPath = path.pathElements[i - 1]
                    rawDistance += Math.sqrt(Math.pow(prevPath.x - nextPath.x, 2)
                                             + Math.pow(prevPath.y - nextPath.y, 2))
                }

            }

            pathAnim.duration = Math.round(rawDistance * 20 * speedFactor)
            if (useMoveTransitions) {
                if (currentCell.xPos < targetCell.xPos) state = "goingRight"
                else if (currentCell.xPos > targetCell.xPos) state = "goingLeft"
                else if (currentCell.yPos < targetCell.yPos) state = "goingDown"
                else if (currentCell.yPos > targetCell.yPos) state = "goingUp"

            }

            pathAnim.start()
            previousCell = currentCell
            currentCell = targetCell
        }
    }

    function setSpeedFactor() {
        switch (age) {
        case Mouse.Age.Pup:
            speedFactor = Utils.getRandomInRange(0.7, 0.8)
            break
        case Mouse.Age.Old:
            speedFactor = Utils.getRandomInRange(1.3, 1.4)
            break
        default:
            speedFactor = Utils.getRandomInRange(1.0, 1.1)
            break
        }
    }



}
