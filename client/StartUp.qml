import QtQuick
import QtQuick.Controls
import QtWebSockets
import my.mazes
import QtQuick.Layouts

import "ui/settings"
import "ui/mazes"

//import "SettingListView.qml" as SettingListView

Window {
    id: startup
    visible: true
    title: qsTr("New simulation")

    width: 1280
    height: 768

    property var selectedMaze

    Component.onCompleted: {
        // maze default connections
        settings.mazeShapeChanged.connect(onMazeShapeChanged)
        settings.graphReceived.connect(onGraphReceived)
        settings.showGraphChanged.connect(onShowGraphChanged)
        settings.clearMaze.connect(circleMaze.clear)
        settings.clearMaze.connect(rectMaze.clear)

        // mouse default connections
        // If we can add a mouse, we know that a maze is ready
//        settings.mouseReady.connect(selectedMaze.addMouse)
//        settings.mouseRequested.connect(requestMouse)
        // start simulation
//        ruleSettings.startClicked.connect(circleMaze.start)
        settings.startClicked.connect(sendConfig)
    }

    onWidthChanged: selectedMaze.repaint()


    RowLayout {
        id: rowLayout
        anchors.fill: parent


        StackLayout {
            id: mazeStack
            Circle {
                id: circleMaze
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: startup.height
                Layout.preferredHeight: startup.height
                width: startup.height
                height: startup.height
            }

            RectMaze {
                id: rectMaze
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.preferredWidth: startup.height
                Layout.preferredHeight: startup.height
                width: startup.height
                height: startup.height
            }
        }

        Settings {
            id: settings
            Layout.fillHeight: true

        }

    }

    WebSocket {
        id: socket
        url: "ws://localhost:4444"
        onTextMessageReceived: (message) => {
                                   console.log("Recieved: ", message)
                                   if (message.indexOf(";") !== -1) {
                                        selectedMaze.onMouseActionReceived(message)
                                   }
        }
        onStatusChanged: if (socket.status == WebSocket.Error) {
                             console.log("Error: " + socket.errorString)
                         } else if (socket.status == WebSocket.Open) {
                             console.log("Socket opened!")
                         } else if (socket.status == WebSocket.Closed) {
                             console.log("Socket closed")
                         }
        active: false
    }


    function onMazeShapeChanged(mazeShape) {
        console.log("Maze: %1".arg(mazeShape))
        if (mazeShape === "Circle") {
            console.log("Circle maze selected")
            mazeStack.currentIndex = 0
            selectedMaze = circleMaze
            rectMaze.clear()
            // food
            settings.addFood.connect(circleMaze.addFood)
            settings.addFood.disconnect(rectMaze.addFood)

            // mouse
            settings.addMouse.connect(circleMaze.addMouse)
            settings.addMouse.disconnect(rectMaze.addMouse)

        } else if (mazeShape === "Rectangle") {
            console.log("Rectangle maze selected")
            mazeStack.currentIndex = 1
            selectedMaze = rectMaze
            circleMaze.clear()
            // food
            settings.addFood.disconnect(circleMaze.addFood)
            settings.addFood.connect(rectMaze.addFood)

            // mouse
            settings.addMouse.disconnect(circleMaze.addMouse)
            settings.addMouse.connect(rectMaze.addMouse)
        }
    }

    function onGraphReceived(graph) {
        if (mazeStack.currentIndex === 0) {
            console.log("Received circle graph!")
            circleMaze.onGraphReceived(graph)
        }
        else if (mazeStack.currentIndex === 1) {
            console.log("Received rectangle graph!")
            rectMaze.onGraphReceived(graph)
        }

    }

    function onShowGraphChanged(shouldShow) {
         if (mazeStack.currentIndex === 0) {
             circleMaze.showGraph(shouldShow)
         } else if (mazeStack.currentIndex === 1) {
             rectMaze.showGraph(shouldShow)
         }
    }

    function prepareConfig() {
        var config = {"adjacencyList": selectedMaze.toAdjacencyList()}
        var mouseList = []
        for (var mId in selectedMaze.miceList) {
            var mouse = selectedMaze.miceList[mId]
//            console.log("Mouse #%1:".arg(mId) + JSON.stringify(mouse.toObject()))
            mouseList.push(mouse.toObject())
        }

        config["mice"] = mouseList

        var foodItems = []

        for (var cId in selectedMaze.foodList) {
            var foodItem = selectedMaze.foodList[cId].toObject()
            foodItems.push(foodItem)
        }

        config["foodList"] = foodItems

        return config
    }

    function sendConfig() {
        var config = JSON.stringify(prepareConfig())
        console.log(config)
        if (!socket.active)
            socket.active = true
        socket.sendTextMessage(config)
    }
}


/*##^##
Designer {
    D{i:0;formeditorZoom:0.0625}
}
##^##*/
