import QtQuick

// Cheese gif from https://www.michellelega.com/pixel-art/

Item {
    property string name
    property alias icon: icon

    property int restoreValue
    property int cellId

    width: 120
    height: 120

    AnimatedImage {
        id: icon
        source: "assets/food/Cheese.gif"
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
    }

    function toObject() {
        return {
            "cellId": cellId,
            "healthRestoreValue": restoreValue
        }
    }

}
