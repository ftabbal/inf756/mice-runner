TEMPLATE = subdirs

SUBDIRS += \
    maze-lib \
    client \
    maze-server

CONFIG += ordered

client.depends = maze-lib
maze-server.depends = maze-lib
