#ifndef MAZE_FACTORY_H
#define MAZE_FACTORY_H

#include <QObject>
#include "grids/base.h"

#include <QJsonObject>

#include "size.h"

namespace Maze {

class Factory : public QObject
{
    Q_OBJECT
public:
    enum class Algo {
        Grid,
        BinaryTree,
        AldousBroder
    };
    Q_ENUM(Algo)

    enum class Shape {
        Rectangle,
        Circle
    };
    Q_ENUM(Shape)

    explicit Factory(QObject *parent = nullptr);
    static Maze::Grid::BaseGrid* makeGrid(Shape shape, std::shared_ptr<Size>);
    static Maze::Grid::BaseGrid* make(Shape shape, Algo algo, std::shared_ptr<Size>);



protected:

};

} // namespace Maze

#endif // MAZE_FACTORY_H
