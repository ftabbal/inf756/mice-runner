#include "algo_base.h"

namespace Maze {
namespace Algorithm {

Base::Base(QObject *parent)
    : QObject{parent}
{

}

QJsonObject Base::toJson() const {
    QJsonObject res;
    res["name"] = m_name;
    res["desc"] = m_desc;
    res["code"] = m_code;
    return res;
}

} // namespace Algorithm
} // namespace Maze
