#ifndef ALGORITHM_BINARYTREE_H
#define ALGORITHM_BINARYTREE_H

#include "algo_base.h"

namespace Maze {
namespace Algorithm {

class BinaryTree : public Base
{
public:
    explicit BinaryTree(QObject *parent = nullptr);
    void applyOn(Maze::Grid::BaseGrid* aGrid);
};

} // namespace Algorithm
} // namespace Maze

#endif // ALGORITHM_BINARYTREE_H
