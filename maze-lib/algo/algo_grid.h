#ifndef MAZE_ALGORITHM_GRID_H
#define MAZE_ALGORITHM_GRID_H

#include "algo_base.h"
#include <QObject>

namespace Maze {
namespace Algorithm {

class Grid : public Maze::Algorithm::Base
{
    Q_OBJECT
public:
    explicit Grid(QObject *parent = nullptr);
    void applyOn(Maze::Grid::BaseGrid* grid) override;
};

} // namespace Algorithm
} // namespace Maze

#endif // MAZE_ALGORITHM_GRID_H
