#include "aldousbroder.h"

#include <QRandomGenerator>

namespace Maze {
namespace Algorithm {

AldousBroder::AldousBroder(QObject *parent)
    : Maze::Algorithm::Base{parent}
{
    m_name = tr("Aldous-Broder");
    m_desc = tr("TODO");
    m_code = "AldousBroder";
}

void AldousBroder::applyOn(Maze::Grid::BaseGrid* grid) {
    auto cell = grid->randomCell();
    size_t unvisited = grid->cells().size() - 1;

    while (unvisited > 0) {
        auto neighbour = cell->randomNeighbour();
        if (!neighbour->hasConnections()) {
            cell->link(neighbour);
            unvisited -= 1;
        }
        cell = neighbour;
    }
}

} // namespace Algorithm
} // namespace Maze
