#include "algo_grid.h"

namespace Maze {
namespace Algorithm {

Grid::Grid(QObject *parent)
    : Maze::Algorithm::Base{parent}
{
    m_name = tr("Grid");
    m_desc = tr("A maze with no walls. Use it to see the possible passages.");
    m_code = "Grid";
}

void Grid::applyOn(Maze::Grid::BaseGrid* grid) {
    for (auto c : grid->cells()) {
        for (auto n : c->neighbours()) {
            c->link(n);
        }
    }
}

} // namespace Algorithm
} // namespace Maze
