#include "binary_tree.h"

#include <QRandomGenerator>

namespace Maze {
namespace Algorithm {

BinaryTree::BinaryTree(QObject *parent)
    : Algorithm::Base{parent}
{
    m_name = tr("Binary Tree");
    m_desc = tr("TODO: Description of binary tree algorithm");
    m_code = "BT";
}

void BinaryTree::applyOn(Maze::Grid::BaseGrid* aGrid) {
    for (auto c : aGrid->cells()) {
        auto neighbours = std::vector<Grid::CellPtr>();
        if (c->hasLowerNeighbour()) neighbours.push_back(c->lowerNeighbour());
        if (c->hasLeftNeighbour()) neighbours.push_back(c->leftNeighbour());
        if (neighbours.size() > 1) {
            auto n_index = QRandomGenerator::global()->bounded(0, static_cast<int>(neighbours.size()));
            auto res = neighbours[n_index];
            c->link(res);
        } else if (neighbours.size() == 1) {
            c->link(neighbours[0]);
        }
    }
}

} // namespace Algorithm
} // namespace Maze
