#ifndef ALGORITHM_BASE_H
#define ALGORITHM_BASE_H

#include <QObject>
#include <QJsonObject>
#include "grids/base.h"

namespace Maze {
namespace Algorithm {

class Base : public QObject
{
    Q_OBJECT
public:
    explicit Base(QObject *parent = nullptr);

    virtual void applyOn(Maze::Grid::BaseGrid* aGrid)  = 0;
    QString name() const {return m_name;}
    QString desc() const {return m_desc;}
    QString code() const {return m_code;}

    QJsonObject toJson() const;

protected:
    QString m_name;
    QString m_desc;
    QString m_code;

};

} // namespace Algorithm
} // namespace Maze

#endif // ALGORITHM_BASE_H
