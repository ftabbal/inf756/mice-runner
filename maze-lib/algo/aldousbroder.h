#ifndef MAZE_ALGORITHM_ALDOUSBRODER_H
#define MAZE_ALGORITHM_ALDOUSBRODER_H

#include "algo_base.h"
#include <QObject>

namespace Maze {
namespace Algorithm {

class AldousBroder : public Maze::Algorithm::Base
{
    Q_OBJECT
public:
    explicit AldousBroder(QObject *parent = nullptr);
    void applyOn(Maze::Grid::BaseGrid* grid) override;
};

} // namespace Algorithm
} // namespace Maze

#endif // MAZE_ALGORITHM_ALDOUSBRODER_H
