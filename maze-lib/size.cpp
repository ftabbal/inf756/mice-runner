#include "size.h"

#include <QStringList>

namespace Maze {

Size::Size()
{

}

SizePtr Size::make(const QString& query) {
    if (!query.contains(",")) {
        auto res = std::make_shared<CircleSize>();
        res->loadString(query);
        return res;
    } else {
        auto res = std::make_shared<DefaultSize>();
        res->loadString(query);

        return res;
    }
}

SizePtr Size::make(const QJsonObject& data) {
    if (data.contains("radius")) {
        auto res = std::make_shared<CircleSize>();
        res->loadJson(data);
        return res;
    } else {
        auto res = std::make_shared<DefaultSize>();
        res->loadJson(data);
        return res;
    }
}


void CircleSize::loadString(const QString& param) {
    m_radius = param.toInt();
}

void CircleSize::loadJson(const QJsonObject& data) {
    m_radius = data["radius"].toInt();
}

// String from the query is "a,b"
void DefaultSize::loadString(const QString& param) {
    QStringList widthAndHeight = param.split(",");
    m_width = widthAndHeight[0].toInt();
    m_height = widthAndHeight[1].toInt();
}

void DefaultSize::loadJson(const QJsonObject& data) {
    m_width = data["w"].toInt();
    m_height = data["h"].toInt();
}

} // namespace Maze
