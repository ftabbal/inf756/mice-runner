#ifndef MAZE_SIZETYPE_H
#define MAZE_SIZETYPE_H

#include <QString>
#include <QJsonObject>


namespace Maze {

class Size;
typedef std::shared_ptr<Size> SizePtr;

class Size
{
public:
    static SizePtr make(const QString& query);
    static SizePtr make(const QJsonObject& data);
protected:
    virtual void loadString(const QString&) = 0;
    virtual void loadJson(const QJsonObject&) = 0;
    Size();
};

class CircleSize : public Size
{
public:
    CircleSize() {}
    int radius() const { return m_radius; }

protected:
    void loadString(const QString&) override;
    void loadJson(const QJsonObject& data) override;
    int m_radius;

    friend Size;
};

class DefaultSize : public Size
{
public:
    DefaultSize() {}
    int width() const { return m_width;}
    int height() const { return m_height; }

protected:
    void loadString(const QString&) override;
    void loadJson(const QJsonObject& data) override;
    int m_width;
    int m_height;

    friend Size;

};

} // namespace Maze

#endif // MAZE_SIZETYPE_H
