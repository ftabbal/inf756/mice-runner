QT -= gui

TEMPLATE = lib
DEFINES += MAZELIB_LIBRARY

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    algo/aldousbroder.cpp \
    algo/algo_base.cpp \
    algo/algo_grid.cpp \
    algo/binary_tree.cpp \
    grids/base.cpp \
    grids/cell.cpp \
    grids/polar/polar.cpp \
    grids/polar/polarcell.cpp \
    grids/rectangle/rectangle.cpp \
    grids/rectangle/rectcell.cpp \
    maze_factory.cpp \
    mazelib.cpp \
    size.cpp

HEADERS += \
    MazeLib_global.h \
    algo/aldousbroder.h \
    algo/algo_base.h \
    algo/algo_grid.h \
    algo/binary_tree.h \
    grids/base.h \
    grids/cell.h \
    grids/polar/polar.h \
    grids/polar/polarcell.h \
    grids/rectangle/rectangle.h \
    grids/rectangle/rectcell.h \
    maze_factory.h \
    mazelib.h \
    size.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
