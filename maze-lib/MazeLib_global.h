#ifndef MAZELIB_GLOBAL_H
#define MAZELIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MAZELIB_LIBRARY)
#  define MAZELIB_EXPORT Q_DECL_EXPORT
#else
#  define MAZELIB_EXPORT Q_DECL_IMPORT
#endif

#endif // MAZELIB_GLOBAL_H
