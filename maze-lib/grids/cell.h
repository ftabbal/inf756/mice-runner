#ifndef MAZE_GRID_CELL_H
#define MAZE_GRID_CELL_H

#include <QObject>

#include <set>

namespace Maze {
namespace Grid {

class Cell;
typedef Cell* CellPtr;

class Cell : public QObject
{
    Q_OBJECT
public:
    explicit Cell(QObject *parent = nullptr) : QObject(parent) {}
    virtual void init(int id, int, int) = 0;
    virtual void link(CellPtr, bool biDirectional = true);
    virtual void unlink(CellPtr, bool biDirectional = true);
    virtual bool isLinked(CellPtr) const;
    bool hasConnections() const { return !m_connections.empty(); }
    virtual void prepareNeighbours() = 0;
    virtual std::set<CellPtr> neighbours() const = 0;
    int id() const { return m_id; }
    std::set<CellPtr> connections() const { return m_connections; }

    QStringList connectionStringList() const;

    CellPtr randomNeighbour() const;

    // neighbours for the binary tree algorithm
    virtual bool hasLowerNeighbour() const = 0;
    virtual bool hasLeftNeighbour() const = 0;
    virtual CellPtr lowerNeighbour() const = 0;
    virtual CellPtr leftNeighbour() const = 0;

protected:
    int m_id;
    std::set<CellPtr> m_connections;
    std::set<CellPtr> m_neighbours;

};

} // namespace Grid
} // namespace Maze

#endif // MAZE_GRID_CELL_H
