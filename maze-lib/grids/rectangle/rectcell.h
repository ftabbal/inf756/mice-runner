#ifndef MAZE_GRID_RECTCELL_H
#define MAZE_GRID_RECTCELL_H

#include "../cell.h"
#include <QObject>

namespace Maze {
namespace Grid {

class RectCell : public Maze::Grid::Cell
{
    Q_OBJECT
public:
    explicit RectCell(QObject *parent = nullptr);
    void init(int id, int xPos, int yPos) override {
        m_id = id;
        m_xPos = xPos;
        m_yPos = yPos;
    }

    void prepareNeighbours() override;
    std::set<CellPtr> neighbours() const override { return m_neighbours; }

    int xPos() const { return m_xPos; }
    int yPos() const { return m_yPos; }

    RectCell* leftCell() const { return m_leftNeighbour; }
    RectCell* rightCell() const { return m_rightNeighbour; }
    RectCell* upCell() const { return m_upNeighbour; }
    RectCell* downCell() const { return m_downNeighbour; }

    void setUpNeighbour(RectCell* other) { m_upNeighbour = other; }
    void setLeftNeighbour(RectCell* other) { m_leftNeighbour = other; }
    void setRightNeighbour(RectCell* other) { m_rightNeighbour = other; }
    void setDownNeighbour(RectCell* other) { m_downNeighbour = other; }

    // neighbours for the binary tree algorithm
    bool hasLowerNeighbour() const override {return m_downNeighbour != nullptr; }
    bool hasLeftNeighbour() const override { return m_leftNeighbour != nullptr; }
    CellPtr lowerNeighbour() const override {return m_downNeighbour; }
    CellPtr leftNeighbour() const override { return m_leftNeighbour; }


protected:
    int m_xPos;
    int m_yPos;

    RectCell* m_upNeighbour;
    RectCell* m_leftNeighbour;
    RectCell* m_rightNeighbour;
    RectCell* m_downNeighbour;

};

} // namespace Grid
} // namespace Maze

#endif // MAZE_GRID_RECTCELL_H
