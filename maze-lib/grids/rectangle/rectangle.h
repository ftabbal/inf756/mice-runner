#ifndef MAZE_GRID_RECTANGLE_H
#define MAZE_GRID_RECTANGLE_H

#include <QObject>
#include "../base.h"

#include "rectcell.h"

namespace Maze {
namespace Grid {

class Rectangle : public BaseGrid
{
    Q_OBJECT
public:
    explicit Rectangle(QObject* parent = nullptr) : BaseGrid(parent) {}
    void init(int height, int width) override;
    int size() const override { return m_width * m_height; }
    QJsonObject toJson() const override;

    int width() const { return m_width; }
    int height() const { return m_height; }


protected:
    void configure() override;

    int m_width;
    int m_height;

    std::vector<std::vector<RectCell*>> m_rows;

};

} // namespace Grid
} // namespace Maze

#endif // MAZE_GRID_RECTANGLE_H
