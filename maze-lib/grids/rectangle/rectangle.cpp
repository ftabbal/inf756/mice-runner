#include "rectangle.h"

#include "rectcell.h"

#include <QJsonArray>

namespace Maze {
namespace Grid {

void Rectangle::init(int height, int width) {
    m_height = height;
    m_width = width;
    int idCounter = 0;
    for (int y = 0; y < m_height; ++y) {
        auto rowCells = std::vector<RectCell*>();
        for (int x= 0; x < m_width; ++x) {
            auto c = new RectCell(this);
            c->init(idCounter, x, y);
            m_cells.push_back(c);
            rowCells.push_back(c);
            idCounter += 1;
        }
        m_rows.push_back(rowCells);
    }

    configure();
}

void Rectangle::configure() {
    for (auto& r : m_rows) {
        for (auto c : r) {
            int x = c->xPos();
            int y = c->yPos();
            if (y > 0) {
                auto upCell = m_rows[y - 1][x];
                c->setUpNeighbour(upCell);
                upCell->setDownNeighbour(c);
            }
            if (y < m_height - 1) {
                auto downCell = m_rows[y + 1][x];
                c->setDownNeighbour(downCell);
                downCell->setUpNeighbour(c);
            }

            if (x > 0) {
                auto leftCell = m_rows[y][x - 1];
                c->setLeftNeighbour(leftCell);
                leftCell->setRightNeighbour(c);
            }

            if (x < m_width - 1) {
                auto rightCell = m_rows[y][x + 1];
                c->setRightNeighbour(rightCell);
                rightCell ->setLeftNeighbour(c);
            }

            c->prepareNeighbours();
        }
    }
}

QJsonObject Rectangle::toJson() const {
    QJsonObject res;
    res["shape"] = "Rectangle";
    QJsonObject size;
    size["w"] = m_width;
    size["h"] = m_height;
    res["size"] = size;
    res["connections"] = QJsonArray::fromStringList(adjacencyList());
    return res;
}

} // namespace Grid
} // namespace Maze
