#include "rectcell.h"

namespace Maze {
namespace Grid {

RectCell::RectCell(QObject *parent)
    : Maze::Grid::Cell{parent}
{

}

void RectCell::prepareNeighbours() {
    if (m_upNeighbour) m_neighbours.insert(m_upNeighbour);
    if (m_downNeighbour) m_neighbours.insert(m_downNeighbour);
    if (m_leftNeighbour) m_neighbours.insert(m_leftNeighbour);
    if (m_rightNeighbour) m_neighbours.insert(m_rightNeighbour);
}

} // namespace Grid
} // namespace Maze
