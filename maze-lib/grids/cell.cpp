#include "cell.h"

#include <QRandomGenerator>

namespace Maze {
namespace Grid {

void Cell::link(CellPtr other, bool biDirectional) {
    m_connections.insert({other});
    if (biDirectional)
        other->m_connections.insert({this});
}

void Cell::unlink(CellPtr other, bool biDirectional) {
    if (m_connections.contains(other)) {
        m_connections.erase(other);
        if (biDirectional) {
            other->unlink(this, biDirectional);
        }
    }
}

bool Cell::isLinked(CellPtr other) const {
    return m_neighbours.contains(other);
}

QStringList Cell::connectionStringList() const {
    QStringList res;
    for (auto n : m_connections)
        res << QString::number(n->id());
    return res;
}

CellPtr Cell::randomNeighbour() const {
    int index = QRandomGenerator::global()->bounded(0, static_cast<int>(m_neighbours.size()));
    auto neighbourList = std::vector<CellPtr>(m_neighbours.begin(), m_neighbours.end());
    return neighbourList[index];
}

} // namespace Grid
} // namespace Maze
