#ifndef MAZE_GRID_BASE_H
#define MAZE_GRID_BASE_H

#include <QObject>
#include "cell.h"
#include <vector>
#include <QJsonObject>
#include <QJsonArray>
#include "size.h"

namespace Maze {
namespace Grid {

class BaseGrid : public QObject
{
    Q_OBJECT
public:
    explicit BaseGrid(QObject* parent = nullptr) : QObject(parent) {}
    virtual void init(int width, int height) = 0;
    QStringList adjacencyList() const;
    void printAdjacencyList();

    std::vector<CellPtr>& cells() { return m_cells; }
    virtual int size() const = 0;

    virtual CellPtr randomCell() const;

    // Serialization
    virtual QJsonObject toJson() const = 0;

    // Deserialization
    virtual void load(const QJsonObject& data);

protected:
    void loadSize(const QJsonObject& data);
    void loadConnections(const QJsonArray& data);
    std::vector<CellPtr> m_cells;
    Maze::SizePtr m_size;
    virtual void configure() = 0;

};

} // namespace Grid
} // namespace Maze

#endif // MAZE_GRID_BASE_H
