#include "polarcell.h"

#include <QRandomGenerator>

namespace Maze {
namespace Grid {

PolarCell::PolarCell(QObject *parent)
    : Maze::Grid::Cell{parent}
{

}

void PolarCell::setCcw(PolarCellPtr ccw) {
    m_ccw = ccw;
    ccw->m_cw = this;
}

void PolarCell::setCw(PolarCellPtr cw) {
    m_cw = cw;
    cw->m_ccw = this;
}

void PolarCell::addInwardCell(PolarCellPtr inwardCell) {
    m_inward.insert({inwardCell});
    inwardCell->m_outward.insert({this});
}

void PolarCell::addOutwardCell(PolarCellPtr outwardCell) {
    m_outward.insert({outwardCell});
    outwardCell->m_inward.insert({this});
}

void PolarCell::prepareNeighbours() {
    if (m_ccw)
        m_neighbours.insert(m_ccw);
    if (m_cw)
        m_neighbours.insert(m_cw);

    for (auto c : m_inward)
        m_neighbours.insert(c);

    for (auto c: m_outward)
        m_neighbours.insert(c);
}

bool PolarCell::hasLowerNeighbour() const {
    return m_inward.size() > 0;
}

bool PolarCell::hasLeftNeighbour() const {
    return m_ccw != nullptr;
}

CellPtr PolarCell::lowerNeighbour() const {
    if (hasLowerNeighbour()) {
        if (m_inward.size() == 1)
            return *m_inward.begin();

        int index = QRandomGenerator::global()->bounded(0, static_cast<int>(m_inward.size()));
        auto inwardCells = std::vector<CellPtr>(m_inward.begin(), m_inward.end());
        return inwardCells[index];
    }
    return nullptr;
}

CellPtr PolarCell::leftNeighbour() const {
    return m_ccw;
}

} // namespace Grid
} // namespace Maze
