#include "polar.h"

#include "polarcell.h"

#include <QJsonObject>
#include <QJsonArray>

namespace Maze {
namespace Grid {

void Polar::init(int numLevels) {
    init(numLevels, 0);
    configure();
}

void Polar::init(int numLevels, int /*unused*/) {
    m_numLevels = numLevels;
    double rowHeight = 1.0 / numLevels;
    int idCounter = 0;
    auto center = new PolarCell(this);
    center->init(0, 0, 0, 0);
    m_cellsByLevel.push_back(std::vector<PolarCellPtr>({center}));
    m_cells.push_back(center);
    for (int level = 1; level < numLevels; ++level) {
        double radius = static_cast<double>(level) / numLevels;
        double circumference = 2 * M_PI * radius;
        size_t previous_count = m_cellsByLevel[level - 1].size();
        double estimated_cell_width = circumference / previous_count;
        int ratio = qRound(estimated_cell_width / rowHeight);
        int numCells = previous_count * ratio;
        double angleSize = 2 * M_PI / numCells;
        auto cellsForLevel = std::vector<PolarCellPtr>();
        for (int index = 0; index < numCells; ++index) {
            float angle = angleSize * index;
            auto cellToAdd = new PolarCell(this);
            cellToAdd->init(++idCounter, level, index, angle);
            cellsForLevel.push_back(cellToAdd);
            m_cells.push_back(cellToAdd);
        }

        m_cellsByLevel.push_back(cellsForLevel);
    }
}

void Polar::configure() {
    for (int lvl = 1; lvl < m_numLevels; ++lvl) {
        for (auto cell : m_cellsByLevel[lvl]) {
            // Using classical polar convention (i.e. counter clockwise > clockwise)
            auto ccw = get(lvl, cell->index() + 1);
            auto cw = get(lvl, cell->index() - 1);
            cell->setCcw(ccw);
            cell->setCw(cw);

            if (lvl == 1) {
                auto origin = get(0, 0);
                cell->addInwardCell(origin);
            } else {
                int ratio = m_cellsByLevel[lvl].size() / m_cellsByLevel[lvl-1].size();
                if (ratio == 1) {
                    auto cellInward = get(lvl - 1, cell->index());
                    cell->addInwardCell(cellInward);
                } else {
                    auto cellInward = get(lvl - 1, cell->index() / ratio);
                    cell->addInwardCell(cellInward);
                    if (cell->index() % 2 != 0) {
                        auto cellInward1 = get(lvl - 1, cell->index() / ratio + 1);
                        cell->addInwardCell(cellInward1);
                    }
                }
            }
        }
    }

    for (auto c : m_cells) c->prepareNeighbours();
}

PolarCellPtr Polar::get(int level, int index) const {
    if (level == 0) {
        return m_cellsByLevel[0].front();
    } else {
        if (index >= 0)
            return m_cellsByLevel[level][index % m_cellsByLevel[level].size()];
        else
            return m_cellsByLevel[level][m_cellsByLevel[level].size() - 1];
    }
}

QJsonObject Polar::toJson() const {
    QJsonObject res;
    res["shape"] = "Circle";
    QJsonObject size;
    size["radius"] = m_numLevels;
    res["size"] = size;
    res["connections"] = QJsonArray::fromStringList(adjacencyList());
    return res;
}

} // namespace Grid
} // namespace Maze
