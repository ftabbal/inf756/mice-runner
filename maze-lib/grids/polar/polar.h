#ifndef MAZE_GRID_POLAR_H
#define MAZE_GRID_POLAR_H

#include <QObject>
#include "polarcell.h"
#include "../base.h"

namespace Maze {
namespace Grid {

class Polar : public BaseGrid
{
    Q_OBJECT
public:
    explicit Polar(QObject* parent = nullptr) : BaseGrid(parent) {}
    void init(int numLevels);
    int size() const override { return m_numLevels;}
    int numCellsForLevel(int level) const { return static_cast<int>(m_cellsByLevel[level].size()); }

    QJsonObject toJson() const override;

protected:
    void init(int numLevels, int /*unused*/) override;
    void configure() override;
    PolarCellPtr get(int level, int index) const;
    int m_numLevels;
    std::vector<std::vector<PolarCellPtr>> m_cellsByLevel;

};

} // namespace Grid
} // namespace Maze

#endif // MAZE_GRID_POLAR_H
