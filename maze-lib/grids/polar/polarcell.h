#ifndef MAZE_GRID_POLARCELL_H
#define MAZE_GRID_POLARCELL_H

#include "../cell.h"

namespace Maze {
namespace Grid {

class PolarCell;
typedef PolarCell* PolarCellPtr;

class PolarCell : public Maze::Grid::Cell
{
public:
    explicit PolarCell(QObject *parent = nullptr);
    void init(int id, int level, int index, double angle) {
        init(id, level, index);
        m_angle = angle;
    }

    int index() const { return m_index;}
    int level() const { return m_level;}
    double angle() const { return m_angle; }
    void setCcw(PolarCellPtr ccw);
    void setCw(PolarCellPtr cw);
    void addInwardCell(PolarCellPtr inwardCell);
    void addOutwardCell(PolarCellPtr outwardCell);

    CellPtr ccwCell() const { return m_ccw; }
    CellPtr cwCell() const { return m_cw; }
    std::set<PolarCellPtr> inwardCells() const { return m_inward; }
    std::set<PolarCellPtr> outwardCells() const { return m_outward; }

    void prepareNeighbours() override;
    std::set<CellPtr> neighbours() const override { return m_neighbours; }

    // neighbours for the binary tree algorithm
    bool hasLowerNeighbour() const override;
    bool hasLeftNeighbour() const override;
    CellPtr lowerNeighbour() const override;
    CellPtr leftNeighbour() const override;


protected:
    void init(int id, int level, int index) override {
        m_id = id;
        m_level = level;
        m_index = index;
    }
    int m_level;
    int m_index;
    double m_angle;
    PolarCellPtr m_ccw;
    PolarCellPtr m_cw;
    std::set<PolarCellPtr> m_inward;
    std::set<PolarCellPtr> m_outward;
};

} // namespace Grid
} // namespace Maze

#endif // MAZE_GRID_POLARCELL_H
