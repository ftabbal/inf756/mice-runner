#include "base.h"

#include <QDebug>

#include <QRandomGenerator>

namespace Maze {
namespace Grid {

CellPtr BaseGrid::randomCell() const {
    int index = QRandomGenerator::global()->bounded(0, static_cast<int>(m_cells.size()));
    return m_cells[index];
}

void BaseGrid::printAdjacencyList() {
    for (auto c : m_cells) {
        qDebug() << c->id() << ":" << c->connectionStringList().join(" - ");
    }
}

QStringList BaseGrid::adjacencyList() const {
    QStringList res;
    for (auto c : m_cells) {
        QStringList entry;
        entry << QString::number(c->id());
        for (auto n : c->connections())
            entry << QString::number(n->id());
        res << entry.join(";");
    }

    return res;
}

void BaseGrid::load(const QJsonObject& data) {
    loadSize(data["size"].toObject());
    loadConnections(data["connections"].toArray());
}

void BaseGrid::loadSize(const QJsonObject& data) {
    m_size = Maze::Size::make(data);
}

void BaseGrid::loadConnections(const QJsonArray& data) {
    for (const QJsonValue& val : data) {

        QString line = val.toString();
        QStringList rowContent = line.split(";");
        auto c = m_cells.at(rowContent[0].toInt());

        for (int i = 1; i < rowContent.size(); ++i) {
            int index = rowContent.at(i).toInt();
            auto n = m_cells.at(index);
            c->link(n);
        }
//        m_graph[rowContent.at(0).toInt()] =  neighbors;
    }
}

} // namespace Grid
} // namespace Maze
