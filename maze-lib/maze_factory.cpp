#include "maze_factory.h"

#include "grids/polar/polar.h"
#include "grids/rectangle/rectangle.h"
#include "algo/binary_tree.h"
#include "algo/algo_grid.h"
#include "algo/aldousbroder.h"

#include <QDebug>

namespace Maze {

Factory::Factory(QObject *parent)
    : QObject{parent}
{

}

Maze::Grid::BaseGrid* Factory::makeGrid(Shape shape, std::shared_ptr<Size> size) {
    switch (shape) {
    case Shape::Circle: {
        Maze::Grid::Polar* res = new Maze::Grid::Polar();
        std::shared_ptr<CircleSize> trueSize = std::dynamic_pointer_cast<CircleSize>(size);
        res->init(trueSize->radius());
        return res;
    }
    case Shape::Rectangle: {
        Maze::Grid::Rectangle* res = new Maze::Grid::Rectangle();
        std::shared_ptr<DefaultSize> trueSize = std::dynamic_pointer_cast<DefaultSize>(size);
        res->init(trueSize->height(), trueSize->width());
        return res;
    }
    }
    qDebug() << shape << "Not implemented!";
    return nullptr;
}


Maze::Grid::BaseGrid* Factory::make(Shape shape, Algo algo, std::shared_ptr<Size> size) {

    Maze::Grid::BaseGrid* res = Factory::makeGrid(shape, size);


    switch (algo) {
    case Algo::Grid: {
        Maze::Algorithm::Grid gr;
        gr.applyOn(res);
        break;
    }
    case Algo::BinaryTree: {
        Maze::Algorithm::BinaryTree bt;
        bt.applyOn(res);
        break;
    }
    case Algo::AldousBroder: {
        Maze::Algorithm::AldousBroder ab;
        ab.applyOn(res);
        break;
    }
    default:
        qDebug() << algo << "Not implemented!";
        return nullptr;
    }


    return res;
}

} // namespace Maze
