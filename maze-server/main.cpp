#include <QCoreApplication>

#include "MazeLib_global.h"
#include "maze_factory.h"

#include "algo/binary_tree.h"
#include "algo/algo_grid.h"
#include "algo/aldousbroder.h"


#include <QHttpServer>
#include <QJsonArray>
#include <QJsonObject>

#include <QDebug>


int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);

    QHttpServer httpServer;

    const auto port = httpServer.listen(QHostAddress::LocalHost, 3001);

    qDebug() << QCoreApplication::translate("QHttpServerExample",
            "Running on http://127.0.0.1:%1/ (Press CTRL+C to quit)").arg(port);

    httpServer.route("/maze", QHttpServerRequest::Method::Get, [] (const QHttpServerRequest &request){
        auto query = request.query();
        // Parse shapes
        auto shapeQuery = query.queryItemValue("shape").toLower();
        Maze::Factory::Shape sh;
        if (shapeQuery == "circle") sh = Maze::Factory::Shape::Circle;
        else if (shapeQuery == "rectangle") sh = Maze::Factory::Shape::Rectangle;
        else {
            qDebug() << "no matching shape for" << shapeQuery;
//            return QHttpServerResponder::StatusCode::NotImplemented;
        }

        // Parse algo
        auto algoQuery = query.queryItemValue("algo").toLower();
        Maze::Factory::Algo algo;
        if (algoQuery == "bt") algo = Maze::Factory::Algo::BinaryTree;
        else if (algoQuery == "grid") algo = Maze::Factory::Algo::Grid;
        else if (algoQuery == "aldousbroder") algo = Maze::Factory::Algo::AldousBroder;
        else {
            qDebug() << "no matching algo for" << algoQuery;
//            return QHttpServerResponder::StatusCode::NotImplemented;
        }

        auto sizeQuery = query.queryItemValue("size");
        auto size = Maze::Size::make(sizeQuery);

        auto grid = Maze::Factory::make(sh, algo, size);
        if (!grid) {
            qDebug() << "Error when generating the maze.";
//            return QHttpServerResponder::StatusCode::NoContent;
        }
        auto result = grid->toJson();
        qDebug() << result;
        return QHttpServerResponse(result);
    });


    httpServer.route("/maze/shapes", QHttpServerRequest::Method::Get, []() {
        auto shapeList = QStringList() << "Circle" << "Rectangle";
        auto result = QJsonArray::fromStringList(shapeList);
        qDebug() << shapeList;
        return QHttpServerResponse(result);
    });

    httpServer.route("/maze/algorithms", QHttpServerRequest::Method::Get, []() {
        auto result = QJsonArray();
        result.append(Maze::Algorithm::BinaryTree().toJson());
        result.append(Maze::Algorithm::AldousBroder().toJson());
        result.append(Maze::Algorithm::Grid().toJson());
        return QHttpServerResponse(result);
    });

    return a.exec();
}
